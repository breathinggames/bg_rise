﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;
using UnityEngine.UI;

namespace BreathingGames.Rise
{	
	/// <summary>
	/// A Retro adventure dedicated class that will load the next level
	/// </summary>
	public class RetroAdventureFinishLevel : FinishLevel 
	{
		/// <summary>
		/// Loads the next level
		/// </summary>
		public override void GoToNextLevel()
		{
			RiseEvent.Trigger(RiseEventTypes.LevelComplete);
			RiseGameEvent.Trigger("Save");
			LevelManager.Instance.SetNextLevel (LevelName);
		}	
	}
}
