﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;
using UnityEngine.SceneManagement;

namespace BreathingGames.Rise
{
	/// <summary>
	/// A class with a simple method to load the next level
	/// </summary>
	public class RetroNextLevel : MonoBehaviour 
	{
		/// <summary>
		/// Asks the level manager to load the next level
		/// </summary>
		public virtual void NextLevel()
		{
			LevelManager.Instance.GotoNextLevel ();
		}		
	}
}
