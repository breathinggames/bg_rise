﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;
using UnityEditor;

namespace BreathingGames.Rise
{	
	public static class RetroAdventureProgressManagerMenu 
	{
		[MenuItem("Tools/Rise/Reset all progress", false,21)]
		/// <summary>
		/// Adds a menu item to enable help
		/// </summary>
		private static void ResetProgress()
		{
			RetroAdventureProgressManager.Instance.ResetProgress ();
		}
	}
}