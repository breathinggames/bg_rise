﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;
using System;

namespace BreathingGames.RiseInventorySystem
{	
	[CreateAssetMenu(fileName = "ArmorItem", menuName = "BreathingGames/Rise/InventoryEngine/ArmorItem", order = 2)]
	[Serializable]
	/// <summary>
	/// Demo class for an example armor item
	/// </summary>
	public class ArmorItem : InventoryItem 
	{
		[Header("Armor")]
		public int ArmorIndex;

		/// <summary>
		/// What happens when the armor is equipped
		/// </summary>
		public override bool Equip()
		{
			base.Equip();
			InventoryDemoGameManager.Instance.Player.SetArmor(ArmorIndex);
            return true;
        }	

		/// <summary>
		/// What happens when the armor is unequipped
		/// </summary>
		public override bool UnEquip()
		{
			base.UnEquip();
			InventoryDemoGameManager.Instance.Player.SetArmor(0);
            return true;
        }		
	}
}