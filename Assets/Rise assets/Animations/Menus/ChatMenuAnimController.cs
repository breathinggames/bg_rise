﻿using UnityEngine;
using UnityEngine.UI;

public class ChatMenuAnimController : MonoBehaviour
{
    public GameObject panel;
    public Text textButton;

    Animator animator;
    bool isOpen;

    void Start()
    {
        textButton.text = "Open Chat";

        animator = panel.GetComponent<Animator>();
    }

    public void OpenAndClosePanel()
    {
        isOpen = animator.GetBool("Open");

        if (!isOpen)
        {
            animator.SetBool("Open", true);
            textButton.text = "Close Chat";
        }
        else
        {
            animator.SetBool("Open", false);
            textButton.text = "Open Chat";
        }
    }
}
