﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;
using System;

namespace BreathingGames.RiseInventorySystem
{	
	[CreateAssetMenu(fileName = "BaseItem", menuName = "BreathingGames/Rise/Inventory/BaseItem", order = 0)]
	[Serializable]
	/// <summary>
	/// Base item class, to use when your object doesn't do anything special
	/// </summary>
	public class BaseItem : InventoryItem 
	{
				
	}
}