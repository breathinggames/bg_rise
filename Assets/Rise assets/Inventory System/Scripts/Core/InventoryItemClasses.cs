﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using BreathingGames.Tools;

namespace BreathingGames.RiseInventorySystem
{
	/// <summary>
	/// The possible classes an item can be a part of
	/// </summary>
	public enum ItemClasses { Neutral, Armor, Weapon, Ammo, HealthBonus }
}