﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;

public class MatchmakingLobby : MonoBehaviourPunCallbacks
{
    private string roomName;
    private int roomSize;
    private List<RoomInfo> roomListings;

    [SerializeField] private Transform roomsContainer;
    [SerializeField] private GameObject lobbyPanel;
    [SerializeField] private GameObject lobbyConnectButton;
    [SerializeField] private GameObject roomListingPrefab;
    [SerializeField] private GameObject onlineGate;
    [SerializeField] private InputField playerNameInput;

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true; // all clients connected to a room will automatically load the scene when the server does        

        lobbyConnectButton.SetActive(true);
        roomListings = new List<RoomInfo>(); // initialize roomlisting

        roomName = "Room " + roomListings.Count + 1;
        roomSize = 1;

        // will need to be loaded in Photon Chat and Voice Chat later
        if (PlayerPrefs.HasKey("Nickname"))
        {
            if (PlayerPrefs.GetString("Nickname") == "")
            {
                PhotonNetwork.NickName = "Player " + Random.Range(0, 1000);
            }
            else
            {
                PhotonNetwork.NickName = PlayerPrefs.GetString("Nickname");
            }
        }
        else
        {
            PhotonNetwork.NickName = "Player " + Random.Range(0, 1000);
        }
        playerNameInput.text = PhotonNetwork.NickName;
        Debug.Log("Name entered at the gate: " + PhotonNetwork.NickName);
    }

    public void PlayerNameUpdate(string nameInput)
    {
        PhotonNetwork.NickName = nameInput;
        PlayerPrefs.SetString("Nickname", nameInput);
    }

    public void JoinLobbyOnClick()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            onlineGate.SetActive(false);
            lobbyPanel.SetActive(true);
            PhotonNetwork.JoinLobby();
        }
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        int tempIndex;
        foreach (RoomInfo room in roomList)
        {
            if (roomListings != null) // try to find existing room listing
            {
                tempIndex = roomListings.FindIndex(ByName(room.Name));
            }
            else
            {
                tempIndex = -1;
            }

            if (tempIndex != -1) // remove listing because it has been closed
            {
                roomListings.RemoveAt(tempIndex);
                Destroy(roomsContainer.GetChild(tempIndex).gameObject);
            }

            if (room.PlayerCount > 0)
            {
                roomListings.Add(room);
                ListRoom(room);
            }
        }
    }

    static System.Predicate<RoomInfo> ByName(string name)
    {
        return delegate (RoomInfo room)
        {
            return room.Name == name;
        };
    }

    void ListRoom(RoomInfo room)
    {
        if (room.IsOpen && room.IsVisible)
        {
            GameObject tempListing = Instantiate(roomListingPrefab, roomsContainer);
            RoomButton tempButton = tempListing.GetComponent<RoomButton>();
            tempButton.SetRoom(room.Name, room.MaxPlayers, room.PlayerCount);
        }
    }

    public void OnRoomNameChanged(string nameIn)
    {
        if (nameIn == null || nameIn == "")
        {
            nameIn = "Room " + roomListings.Count;
        }
        roomName = nameIn;
    }

    public void OnRoomSizeChanged(string sizeIn)
    {
        if (sizeIn == null || sizeIn == "" || sizeIn == "0")
        {
            sizeIn = "1";
        }
        roomSize = int.Parse(sizeIn);
    }

    public void CreateRoom()
    {
        Debug.Log("Trying to create a room...");
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)roomSize };
        PhotonNetwork.CreateRoom(roomName, roomOps);
        Debug.Log(roomName);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to create a room but failed. Trying again...");
        
        // print a message in game to tell the player to retry creating a room since the name already exists
    }

    public void OnCancelMatchmaking()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            Debug.Log("The cancel request button was clicked");
            lobbyPanel.SetActive(false);
            onlineGate.SetActive(true);
            if (PhotonNetwork.InLobby)
            {
                PhotonNetwork.LeaveLobby();
            }
            
        }
    }
}
