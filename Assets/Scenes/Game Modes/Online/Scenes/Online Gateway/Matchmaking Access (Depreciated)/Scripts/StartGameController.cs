﻿using Photon.Pun;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartGameController : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject startGame;
    [SerializeField] private GameObject disconnectRequest;

    private void Start()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            startGame.SetActive(true);
            startGame.GetComponent<Button>().interactable = true;
        }
    }

    public void OnStartGameButtonClicked()
    {
        Debug.Log("Start game button was clicked");
        startGame.GetComponent<Button>().interactable = false;

        PhotonNetwork.LoadLevel(MultiplayerSetting.multiplayerSetting.multiplayerGameScene);
    }

    public void DisconnectPlayer()
    {
        StartCoroutine(DisconnectAndLoad());
    }

    IEnumerator DisconnectAndLoad()
    {
        PhotonNetwork.Disconnect();
        while (PhotonNetwork.IsConnected)
        {
            yield return null;
        }
        SceneManager.LoadScene(MultiplayerSetting.multiplayerSetting.lobby);
    }
}
