﻿using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class CharacterSelectionController : MonoBehaviour
{
    private PhotonView PV;
    public Button[] buttons;

    private int[] nb = { -1, -1, -1, -1 };


    int[] networkCheck = { -1, -1 };
    int[] localCheck = { -1, -1 };


    void Start()
    {
        PV = GetComponent<PhotonView>();
    }

    public void SelectCharacter(int characterIdentifier)
    {
        PV.RPC("OnClickCharacterPick", RpcTarget.AllBufferedViaServer, characterIdentifier);
        Debug.Log("I clicked on a Character number " + characterIdentifier);
    }

    public void OnClickCharacterPick(int characterIdentifier)
    {
        if (PlayerInfo.playerInfo != null)
        {
            PlayerInfo.playerInfo.mySelectedCharacter = characterIdentifier;

            for (int i = 0; i < nb.Length; i++)
            {               
                if (localCheck[0] == -1)
                {
                    localCheck[0] = characterIdentifier;
                    buttons[characterIdentifier].interactable = false;
                }
                else if (localCheck[0] != -1 && localCheck[1] == -1)
                {
                    buttons[characterIdentifier].interactable = false;
                    buttons[localCheck[0]].interactable = true;

                    localCheck[0] = localCheck[1];
                    localCheck[1] = -1;
                }

                if (i == characterIdentifier)
                {
                    if (networkCheck[0] == -1)
                    {
                        networkCheck[0] = characterIdentifier;
                        PV.RPC("DeactivateSelection", RpcTarget.AllBufferedViaServer, networkCheck[0]);
                    }
                    else if(networkCheck[0] != -1 && networkCheck[1] == -1)
                    {
                        networkCheck[1] = characterIdentifier;
                        PV.RPC("DeactivateSelection", RpcTarget.AllBufferedViaServer, networkCheck[1]);
                        PV.RPC("ActivateSelection", RpcTarget.AllBufferedViaServer, networkCheck[0]);
                        networkCheck[0] = networkCheck[1];
                        networkCheck[1] = -1;
                    }
                }
                else
                {

                }



                if (PV.IsMine)
                {
                    PlayerPrefs.SetInt("LOCALCHARACTER", characterIdentifier);
                }
            }
        }
    }

    [PunRPC]
    public void DeactivateSelection(int characterIdentifier)
    {
        buttons[characterIdentifier].interactable = false;
    }

    [PunRPC]
    public void ActivateSelection(int characterIdentifier)
    {
        buttons[characterIdentifier].interactable = true;
    }
}
