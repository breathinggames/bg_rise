﻿using Photon.Pun;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PhotonGameSetupQuickAccess : MonoBehaviour
{
    public Transform[] spawnPoints;

    void Start()
    {
        CreatePlayer();
    }

    private void CreatePlayer()
    {
        int counter = PlayerInfo.playerInfo.spawnPrefabNumber;
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", PlayerInfo.playerInfo.avatarPrefabSelected), spawnPoints[counter].transform.position, Quaternion.identity, 0);
    }

    public void DisconnectPlayer()
    {
        StartCoroutine(DisconnectAndLoad());
    }

    public void LeaveRoomWithouthDisconnecting()
    {
        StartCoroutine(LeaveRoom());
    }

    IEnumerator DisconnectAndLoad()
    {
        PhotonNetwork.Disconnect();
        while (PhotonNetwork.IsConnected)
        {
            yield return null;
        }
        SceneManager.LoadScene(MultiplayerSetting.multiplayerSetting.lobby);
    }

    IEnumerator LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        while (PhotonNetwork.InRoom)
        {
            yield return null;
        }
        SceneManager.LoadScene(MultiplayerSetting.multiplayerSetting.lobby);
    }
}
