﻿using System.IO;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PhotonWaitingRoom : MonoBehaviourPunCallbacks, IInRoomCallbacks
{
    // room info
    private PhotonView PV;

    public int playerCountInRoom;
    public Transform[] spawnPoints;

    [SerializeField] private Text playerCountInRoomDisplay;
    [SerializeField] private Text timerToStartGameDisplay;

    [SerializeField] private float maxWaitTime;
    [SerializeField] private float maxFullGameWaitTime;

    private bool readyToCountdown;
    private bool readyToStart;
    private bool startingGame;

    private float timerToStartGame;
    private float notFullGameTimer;
    private float fullGameTimer;

    private int roomSize;

    void Start()
    {
        PV = GetComponent<PhotonView>();

        fullGameTimer = maxFullGameWaitTime;
        notFullGameTimer = maxWaitTime;
        timerToStartGame = maxWaitTime;

        CreatePlayer();
        PlayerCountUpdate();
    }

    private void CreatePlayer()
    {
        int counter = PlayerInfo.playerInfo.spawnPrefabNumber;
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", PlayerInfo.playerInfo.avatarPrefabSelected), spawnPoints[counter].transform.position, Quaternion.identity, 0);
    }

    void PlayerCountUpdate()
    {
        playerCountInRoom = PhotonNetwork.PlayerList.Length;
        roomSize = MultiplayerSetting.multiplayerSetting.maxPlayersInRoomAllowed;
        playerCountInRoomDisplay.text = playerCountInRoom + ":" + roomSize;

        if (playerCountInRoom == roomSize)
        {
            readyToStart = true;
        }
        else if (playerCountInRoom >= MultiplayerSetting.multiplayerSetting.minPlayersInRoomToStartGame)
        {
            readyToCountdown = true;
        }
        else
        {
            readyToCountdown = false;
            readyToStart = false;
        }
    }

    void Update()
    {
        WaitingForMorePlayers();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        PlayerCountUpdate();

        if (PhotonNetwork.IsMasterClient)
        {
            PV.RPC("RPC_SendTimer", RpcTarget.Others, timerToStartGame);
        }
    }

    [PunRPC]
    private void RPC_SendTimer(float timeIn)
    {
        // sync the coundown timer for the player joining the game
        timerToStartGame = timeIn;
        notFullGameTimer = timeIn;

        if (timeIn < fullGameTimer)
        {
            fullGameTimer = timeIn;
        }
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        PlayerCountUpdate();
    }

    void WaitingForMorePlayers()
    {
        if (playerCountInRoom <= 1)
        {
  //          ResetTimer();
        }

        if (readyToStart)
        {
            fullGameTimer -= Time.deltaTime;
            timerToStartGame = fullGameTimer;
        }
        else if (readyToCountdown)
        {
            notFullGameTimer -= Time.deltaTime;
            timerToStartGame = notFullGameTimer;
        }

        //format timer
        string tempTimer = string.Format("{0:00}", timerToStartGame);
        timerToStartGameDisplay.text = tempTimer;

        // at 0 the game starts
        if (timerToStartGame <= 0f && playerCountInRoom == roomSize)
        {
            if (startingGame)
            {
                return;
            }

            StartGame();
        }
        else if(timerToStartGame <= 0f)
        {
            DelayCancel();
        }
    }

    void ResetTimer()
    {
        timerToStartGame = maxWaitTime;
        notFullGameTimer = maxWaitTime;
        fullGameTimer = maxFullGameWaitTime;
    }

    private void StartGame()
    {
        Debug.Log("Loading level....");

        startingGame = true;

        if (!PhotonNetwork.IsMasterClient)
        {
            return;
        }

 //       PhotonNetwork.CurrentRoom.IsOpen = false;
        PhotonNetwork.LoadLevel(MultiplayerSetting.multiplayerSetting.multiplayerGameScene);
    }

    public void DelayCancel()
    {
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene(MultiplayerSetting.multiplayerSetting.lobby);
    }
}
