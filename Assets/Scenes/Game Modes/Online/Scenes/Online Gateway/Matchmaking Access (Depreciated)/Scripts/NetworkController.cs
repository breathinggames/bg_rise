﻿using Photon.Pun;
using System.Collections;
using UnityEngine;

public class NetworkController : MonoBehaviourPunCallbacks
{
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings(); // connect to photon master server
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Player has connected to the " + PhotonNetwork.CloudRegion + " Photon master server");
    }

    public void UpdateServerRegion(string region)
    {
        Debug.Log(region);
    }

    public void DisconnectPlayer()
    {
        StartCoroutine(Disconnect());
    }

    IEnumerator Disconnect()
    {
        PhotonNetwork.Disconnect();
        while (PhotonNetwork.IsConnected)
        {
            yield return null;
        }
    }
}
