﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class RiseSingleton : MonoBehaviour
{
    public static RiseSingleton riseSingleton;

    Scene scene;

    void Awake()
    {
        if (RiseSingleton.riseSingleton == null)
        {
            RiseSingleton.riseSingleton = this;
        }
        else
        {
            if (RiseSingleton.riseSingleton != this)
            {
                Destroy(this.gameObject);
            }
        }

        scene = SceneManager.GetActiveScene();

        if (scene.name == "Game Mode Selection")
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }
}
