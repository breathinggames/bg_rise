﻿#if UNITY_STANDALONE_WIN
using System.IO.Ports;
#endif

using UnityEngine;
using BreathingGames.Rise;

#if UNITY_STANDALONE_WIN

/// <summary>
/// This class manages the device that gets the pressure / flow from the PEP.
/// The input selection is done in BuildAndStart() in Assets/Scripts/Controller/LevelController.cs.
/// </summary>
public class PepInputController_S : InputController_I
{

    public static string arduinoValue;
	private float strongBreathValue;
	SerialPort stream = new SerialPort("com7", 9600); // 115200

	private float calibrationFactor = 1.0f;

    public PepInputController_S(float strongBreathValue)
	{
		this.strongBreathValue = strongBreathValue;
	}

	/// <summary>
	/// Gets the strength of expiration. 0.0f means that the patient is not blowing.
	/// </summary>
	public float GetStrength()
	{
		return strongBreathValue;
	}

	/// <summary>
	/// Gets the BreathingState (expiration, inspiration, holding breath, ...).
	/// </summary>
	public BreathingState GetInputState(){
		if (this.strongBreathValue > 0.2f) {           
            return BreathingState.EXPIRATION;
		} else {
            return BreathingState.INSPIRATION; // HOLDING_BREATH
		}
	}

	/// <summary>
	/// Determines whether the patient is holding the moving input or not (if the right arrow of the keyboard is down or not).
	/// </summary>
	public bool IsMoving(){
		return Input.GetKey (KeyCode.RightArrow);
	}

	void InputController_I.Update()
	{
		stream.Open();

		// timeout if no data received, in miliseconds
		stream.ReadTimeout = 100;
		if (stream.IsOpen) { // stream.available() > 0; ?
			try {
				// reads serial port
				arduinoValue = stream.ReadLine();

				this.strongBreathValue = float.Parse(arduinoValue);
				this.strongBreathValue = (this.strongBreathValue - 6);  // adapt the value received to match the breath
			}
			catch (System.Exception) {	

			}
		}

        if (strongBreathValue >= 0.2f)
        {
            Debug.Log("BOU");
            CharacterJetpack.action = true;
            CharacterHandleWeapon.action = true;
            SpiroControlledBlock.action = true;
        }
        else
        {
            Debug.Log("BOB");
            CharacterJetpack.action = false;
            CharacterHandleWeapon.action = false;
            SpiroControlledBlock.action = false;
        }

   //   Debug.Log ("ard " + arduinoValue);
	//	Debug.Log ("sbv " + this.strongBreathValue);
		stream.Close();
	}


	public void SetCalibrationFactor(float calibrationFactor)
	{
		this.calibrationFactor = calibrationFactor;
	}

	public float GetCalibrationFactor()
	{
		return calibrationFactor;
	}

	public float GetMovingDirection() {
		if (Input.GetKey(KeyCode.LeftArrow)) {
			return 1f;
		}
		if (Input.GetKey(KeyCode.RightArrow)) {
			return -1f;
		}
		return 0f;
	}
}

#endif