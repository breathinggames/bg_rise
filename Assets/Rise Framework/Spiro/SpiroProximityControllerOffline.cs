﻿using UnityEngine;

public class SpiroProximityControllerOffline : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Player Girl Offline with Spiro - Player1")
        {
            SpiroControlledBlock.proximity = true;
        }
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.name == "Player Girl Offline with Spiro - Player1")
        {
            SpiroControlledBlock.proximity = true;
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name == "Player Girl Offline with Spiro - Player1")
        {
            SpiroControlledBlock.proximity = false;
        }
    }
}
