﻿using UnityEngine;

public class MinimapManager : MonoBehaviour
{
    public GameObject minimapSmall;
    public GameObject minimapLarge;

    private bool switchCamMode;

    [SerializeField] private KeyCode minimapInput;

    void Awake()
    {
        if (minimapSmall != null)
        {
            minimapSmall.GetComponent<GameObject>();
        }

        if (minimapLarge != null)
        {
            minimapLarge.GetComponent<GameObject>();
        }

        switchCamMode = true;
    }

    void Update()
    {
        SwitchMinimapMode();
    }

    /// <summary>
    /// Used to change minimap mode when active
    /// </summary>
    public void SwitchMinimapMode()
    {
        if (Input.GetKeyDown(minimapInput))
        {
            if (minimapSmall != null && minimapLarge != null)
            {
                if (switchCamMode)
                {
                    minimapSmall.SetActive(false);
                    minimapLarge.SetActive(true);
                    switchCamMode = false;
                }
                else if (!switchCamMode)
                {
                    minimapLarge.SetActive(false);
                    minimapSmall.SetActive(true);
                    switchCamMode = true;
                }
            }
            else if(minimapSmall == null && minimapLarge != null)
            {
                if (switchCamMode)
                {
                    minimapLarge.SetActive(true);
                    switchCamMode = false;
                }
                else if (!switchCamMode)
                {
                    minimapLarge.SetActive(false);
                    switchCamMode = true;
                }
            }
        }
    }
}
