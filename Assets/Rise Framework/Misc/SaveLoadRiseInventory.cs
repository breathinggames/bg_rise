﻿using BreathingGames.Tools;
using UnityEngine;

public class SaveLoadRiseInventory : MonoBehaviour
{
    public void SaveRiseInventory()
    {
        RiseEventManager.TriggerEvent(new RiseGameEvent("Save"));
        Debug.Log("Game was saved");
    }

    public void LoadRiseInventory()
    {
        RiseEventManager.TriggerEvent(new RiseGameEvent("Load"));
        Debug.Log("Game was loaded");
    }
}
