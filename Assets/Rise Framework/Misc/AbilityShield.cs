﻿using System.Collections;
using UnityEngine;
using Photon.Pun;


public class AbilityShield : MonoBehaviourPunCallbacks, IPunObservable {



    public float shieldDuration = 5.0f;
    public float nextShield;
    public float shieldCoolDown = 5.0f;
    public bool isShieldOn = false;


    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        if (stream.IsWriting) {
            // We own this player: send the others our data
            stream.SendNext(isShieldOn);
        }
        else {
            // Network player, receive data
            this.isShieldOn = (bool)stream.ReceiveNext();
        }
    }


    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        if (this.photonView.IsMine) { 
            if (Input.GetKeyDown(KeyCode.F) && Time.time > nextShield) {
                nextShield = Time.time + shieldCoolDown;
                isShieldOn = true;
            }
        }

        if (isShieldOn) {
            isShieldOn = false;
            ActivateShield();
        }
    }

    public void ActivateShield() {
        var plist = GameObject.FindGameObjectsWithTag("Player");

        foreach (GameObject otherPlayer in plist) {
            StartCoroutine(Shield(otherPlayer));
        }
    }

    private IEnumerator Shield(GameObject otherPlayer) {

        Debug.Log(otherPlayer.name);
        otherPlayer.GetComponent<BreathingGames.Rise.Health>().Invulnerable = true;

        //For now we change the color of the sprite, we'll add particle effects later
        var sprite = otherPlayer.GetComponent<SpriteRenderer>();
        var initialColor = sprite.color;
        sprite.color = Color.yellow;

        yield return new WaitForSeconds(shieldDuration);

        otherPlayer.GetComponent<BreathingGames.Rise.Health>().Invulnerable = false;
        sprite.color = initialColor;
    }

    public void StartCoolDown() {
        shieldCoolDown -= 1.0f * Time.deltaTime;
    }
}
