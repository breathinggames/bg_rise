﻿using BreathingGames.Tools;
using UnityEngine.SceneManagement;
using UnityEngine;
using System;

namespace BreathingGames.Rise
{
    [System.Serializable]
    /// <summary>
    /// A serializable entity to store Rise scenes, whether they've been completed, unlocked, 
    /// how many stars were collected, and which ones
    /// </summary>
    public class RiseScene
    {
        public string SceneName;
        public bool LevelComplete = false;
        public bool LevelUnlocked = false;
        public int MaxSecrets;
        public bool[] CollectedSecrets;
    }

    [System.Serializable]
    /// <summary>
    /// A serializable entity used to store progress : a list of scenes with their internal status (see above)
    /// </summary>
    public class RiseProgress
    {
        public RiseScene[] Scenes;
    }

    /// <summary>
    /// The RiseProgressManager implement progress in the game.
    /// </summary>
    public class RiseProgressManager : Singleton<RiseProgressManager>, RiseEventListener<RiseSecretEvent>, RiseEventListener<RiseEvent>
    {

        // the list of scenes that we'll want to consider for our game
        public RiseScene[] Scenes;

        [InspectorButton("CreateRiseSaveGame")]
        // A button to test creating the save file
        public bool CreateRiseSaveGameBtn;

        // the current amount of collected secret
        public float CurrentCollectedSecret { get; protected set; }

        protected const string _saveFolderName = "RiseProgress";
        protected const string _saveFileName = "RiseProgress.data";

        /// <summary>
        /// On awake, we load our progress and initialize our stars counter
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
            LoadSavedProgress();
            InitializeSecrets();
        }

        /// <summary>
        /// When a level is completed, we update our progress
        /// </summary>
        protected virtual void LevelComplete()
        {
            for (int i = 0; i < Scenes.Length; i++)
            {
                if (Scenes[i].SceneName == SceneManager.GetActiveScene().name)
                {
                    Scenes[i].LevelComplete = true;
                    Scenes[i].LevelUnlocked = true;
                    if (i < Scenes.Length - 1)
                    {
                        Scenes[i + 1].LevelUnlocked = true;
                    }
                }
            }
        }

        private void InitializeSecrets()
        {
            foreach (RiseScene scene in Scenes)
            {
                if (scene.SceneName == SceneManager.GetActiveScene().name)
                {
                    int secrets = 0;
                    foreach (bool secret in scene.CollectedSecrets)
                    {
                        if (secret) { secrets++; }
                    }
                    CurrentCollectedSecret = secrets;
                }
            }
        }

        /// <summary>
        /// Saves the progress to a file
        /// </summary>
        protected virtual void SaveProgress()
        {
            RiseProgress progress = new RiseProgress();
            progress.Scenes = Scenes;

            SaveLoadManager.Save(progress, _saveFileName, _saveFolderName);
        }

        /// <summary>
        /// A test method to create a test save file at any time from the inspector
        /// </summary>
        protected virtual void CreateSaveGame()
        {
            SaveProgress();
        }

        /// <summary>
        /// Loads the saved progress into memory
        /// </summary>
        protected virtual void LoadSavedProgress()
        {
            RiseProgress progress = (RiseProgress)SaveLoadManager.Load(_saveFileName, _saveFolderName);
            if (progress != null)
            {
                Scenes = progress.Scenes;
            }
        }






        /// <summary>
        /// When we grab a secret event, we update our scene status accordingly
        /// </summary>
        /// <param name="corgiSecretEvent">Corgi star event.</param>
        public virtual void OnRiseEvent(RiseSecretEvent corgiSecretEvent)
        {
            foreach (RiseScene scene in Scenes)
            {
                if (scene.SceneName == corgiSecretEvent.SceneName)
                {
                    scene.CollectedSecrets[corgiSecretEvent.SecretID] = true;
                    CurrentCollectedSecret++;
                }
            }
        }








        /// <summary>
        /// When we grab a level complete event, we update our status, and save our progress to file
        /// </summary>
        /// <param name="gameEvent">Game event.</param>
        public virtual void OnRiseEvent(RiseEvent gameEvent)
        {
            switch (gameEvent.EventType)
            {
                case RiseEventTypes.LevelComplete:
                    LevelComplete();
                    SaveProgress();
                    break;
                case RiseEventTypes.GameOver:
                    GameOver();
                    break;
            }
        }

        /// <summary>
        /// This method describes what happens when the player loses all lives. In this case, we reset its progress and all lives will be reset.
        /// </summary>
        protected virtual void GameOver()
        {
            ResetProgress();
        }

        /// <summary>
        /// A method used to remove all save files associated to progress
        /// </summary>
        public virtual void ResetProgress()
        {
            SaveLoadManager.DeleteSaveFolder("RiseProgress");
        }
    }
}