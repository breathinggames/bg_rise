﻿using Photon.Pun;
using UnityEngine;

public class DestroyServerAssetFromScene : MonoBehaviour
{
    private PhotonView PV;

    public bool hasBeenDestroyed;

    void Start()
    {
        PV = GetComponent<PhotonView>();
        hasBeenDestroyed = false;
    }

    private void Update()
    {
//        hasBeenDestroyed = true;

        if (hasBeenDestroyed)
        {
            PV.RPC("InstantKill", RpcTarget.AllBufferedViaServer);
        }
    }

    public virtual void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "DestructibleObject")
        {
            PV.RPC("InstantKill", RpcTarget.AllBufferedViaServer);
        }
    }

    [PunRPC]
    public void InstantKill()
    {
        gameObject.SetActive(false);
    }
}
