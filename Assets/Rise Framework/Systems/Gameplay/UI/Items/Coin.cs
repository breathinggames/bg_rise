using UnityEngine;
using System.Collections;
using BreathingGames.Tools;

namespace BreathingGames.Rise
{
	/// <summary>
	/// Coin manager
	/// </summary>
	[AddComponentMenu("Rise/Items/Coin")]
	public class Coin : PickableItem
    {
        [Header("Coin")]
        /// The amount of points to add when collected
        public int PointsToAdd = 10;

		/// <summary>
		/// Triggered when something collides with the coin
		/// </summary>
		/// <param name="collider">Other.</param>
		protected override void Pick() 
		{
			// we send a new points event for the GameManager to catch (and other classes that may listen to it too)
			RisePointsEvent.Trigger(PointsMethods.Add, PointsToAdd);
		}
	}
}