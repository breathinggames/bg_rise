﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;
using BreathingGames.RiseInventorySystem;
using BreathingGames.Response;

namespace BreathingGames.Rise
{
	public class InventoryPickableItem : ItemPicker 
	{
		[Header("Inventory Pickable Item")]
        /// the Feedback to play when the object gets picked
        public RiseResponses PickFeedbacks;

        protected override void PickSuccess()
		{
			base.PickSuccess ();
			Effects ();
		}

		/// <summary>
		/// Triggers the various pick effects
		/// </summary>
		protected virtual void Effects()
		{
			if (!Application.isPlaying)
			{
				return;
			}				
			else
			{
                PickFeedbacks?.PlayResponses();
			}
		}
	}
}
