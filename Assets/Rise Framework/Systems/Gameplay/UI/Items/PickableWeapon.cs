using UnityEngine;
using System.Collections;

namespace BreathingGames.Rise
{
	/// <summary>
	/// Add this class to a collectible to have the player change weapon when collecting it
	/// </summary>
	[AddComponentMenu("Rise/Items/Pickable Weapon")]
	public class PickableWeapon : PickableItem
	{
		/// the new weapon the player gets when collecting this object
		public Weapon WeaponToGive;

		/// <summary>
		/// What happens when the weapon gets picked
		/// </summary>
		protected override void Pick()
		{
			CharacterHandleWeapon characterShoot = _pickingCollider.GetComponent<CharacterHandleWeapon>();
			characterShoot.ChangeWeapon(WeaponToGive, null);
			if (characterShoot != null)
			{
				if (characterShoot.CanPickupWeapons)
				{
				}
			}	
		}

		/// <summary>
		/// Checks if the object is pickable.
		/// </summary>
		/// <returns>true</returns>
		/// <c>false</c>
		protected override bool CheckIfPickable()
		{
			_character = _pickingCollider.GetComponent<RiseCharacter>();

			// if what's colliding with the coin ain't a characterBehavior, we do nothing and exit
			if ((_character == null) || (_pickingCollider.GetComponent<CharacterHandleWeapon>() == null))
			{
				return false;
			}
			if (_character.CharacterType != RiseCharacter.CharacterTypes.Player)
			{
				return false;
			}
			return true;
		}
	}
}