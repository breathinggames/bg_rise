﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;
using System;
using System.Collections.Generic;
using BreathingGames.Response;
using UnityEngine.Audio;

namespace BreathingGames.Rise
{
    [AddComponentMenu("")]
    [ResponsePathAttribute("Sounds/RiseSound")]
    [ResponseHelpAttribute("This response lets you play a sound through the SoundManager")]
    public class RiseResponseSound : RiseResponse
    {
        [Header("Engine Sound")]
        public AudioClip SoundFX;
        public bool Loop = false;
        
        protected AudioSource _audioSource;

        protected override void CustomPlayResponse(Vector3 position, float attenuation = 1.0f)
        {
            if (Active)
            {
                if (SoundFX != null)
                {
                    _audioSource = SoundManager.Instance.PlaySound(SoundFX, transform.position, Loop);
                }
            }
        }
        
        /// <summary>
        /// This method describes what happens when the response gets stopped
        /// </summary>
        /// <param name="position"></param>
        /// <param name="attenuation"></param>
        protected override void CustomStopResponse(Vector3 position, float attenuation = 1.0f)
        {
            if (Loop)
            {
                SoundManager.Instance.StopLoopingSound(_audioSource);
            }
            _audioSource = null;
        }
    }
}
