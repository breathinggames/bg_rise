﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;
using System;
using System.Collections.Generic;
using UnityEngine.Audio;
using BreathingGames.Response;

namespace BreathingGames.Rise
{
    public class MusicSwitch : MonoBehaviour
    {
        public virtual void On()
        {
            SoundManager.Instance.MusicOn();
        }

        public virtual void Off()
        {
            SoundManager.Instance.MusicOff();
        }        
    }
}
