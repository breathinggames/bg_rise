﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;
using System;
using System.Collections.Generic;
using UnityEngine.Audio;
using BreathingGames.Response;

namespace BreathingGames.Rise
{
    public class SfxSwitch : MonoBehaviour
    {
        public virtual void On()
        {
            SoundManager.Instance.SfxOn();
        }

        public virtual void Off()
        {
            SoundManager.Instance.SfxOff();
        }
    }
}
