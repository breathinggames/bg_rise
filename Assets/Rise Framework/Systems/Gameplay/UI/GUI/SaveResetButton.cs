﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;
using System.Collections.Generic;
using BreathingGames.RiseInventorySystem;

namespace BreathingGames.Rise
{
    public class SaveResetButton : MonoBehaviour
    {
        public virtual void ResetAllSaves()
        {
            SaveLoadManager.DeleteSaveFolder("Achievements");
            SaveLoadManager.DeleteSaveFolder("RetroAdventureProgress");
            SaveLoadManager.DeleteSaveFolder("InventoryEngine");
            SaveLoadManager.DeleteSaveFolder("Rise");
        }		
	}
}