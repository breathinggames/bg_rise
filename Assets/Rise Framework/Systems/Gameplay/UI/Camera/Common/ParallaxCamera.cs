﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;

namespace BreathingGames.Rise
{	
	/// <summary>
	/// Add this class to a camera to have it support parallax layers
	/// </summary>
	[AddComponentMenu("Rise/Camera/Parallax Camera")]
	public class ParallaxCamera : MonoBehaviour 
	{	
		[Information("If you set MoveParallax to true, the camera movement will cause parallax elements to move accordingly.",BreathingGames.Tools.InformationAttribute.InformationType.Info,false)]
		public bool MoveParallax=true;
	}
}