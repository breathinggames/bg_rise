﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;
using System;
using BreathingGames.RiseInventorySystem;

namespace BreathingGames.Rise
{	
	[CreateAssetMenu(fileName = "InventoryKey", menuName = "BreathingGames/Rise/InventoryKey", order = 1)]
	[Serializable]
	/// <summary>
	/// Pickable key item
	/// </summary>
	public class InventoryKey : InventoryItem 
	{
		/// <summary>
		/// When the item is used, we try to grab our character's Health component, and if it exists, we add our health bonus amount of health
		/// </summary>
		public override bool Use()
		{
			base.Use();

            return true;
		}
	}
}