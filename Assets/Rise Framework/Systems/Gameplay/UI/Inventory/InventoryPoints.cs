﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;
using System;
using BreathingGames.RiseInventorySystem;

namespace BreathingGames.Rise
{	
	[CreateAssetMenu(fileName = "InventoryPoints", menuName = "BreathingGames/Rise/InventoryPoints", order = 2)]
	[Serializable]
	/// <summary>
	/// Pickable health item
	/// </summary>
	public class InventoryPoints : InventoryItem 
	{
		[Header("Points")]
		[Information("Here you need to specify the amount of points that need to be gained when picking this item.",InformationAttribute.InformationType.Info,false)]
		/// The amount of points to add when collected
		public int PointsToAdd = 10;

		/// <summary>
		/// When the item is picked, we add points
		/// </summary>
		public override bool Pick()
		{
			base.Pick ();
			// we send a new points event for the GameManager to catch (and other classes that may listen to it too)
			RisePointsEvent.Trigger(PointsMethods.Add, PointsToAdd);
            return true;
		}
	}
}
