﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;
using System;
using BreathingGames.RiseInventorySystem;
using BreathingGames.Response;

namespace BreathingGames.Rise
{	
	[CreateAssetMenu(fileName = "InventoryTimeModifier", menuName = "BreathingGames/Rise/InventoryTimeModifier", order = 2)]
	[Serializable]
	/// <summary>
	/// Pickable health item
	/// </summary>
	public class InventoryTimeModifier : InventoryItem 
	{
		[Header("Time Modifier")]
		[Information("Here you need to specify the new time speed and the duration for which the timescale will be changed.",InformationAttribute.InformationType.Info,false)]
		/// the time speed to apply while the effect lasts
		public float TimeSpeed = 0.5f;
		/// how long the duration will last , in seconds
		public float Duration = 1.0f;

		protected WaitForSeconds _changeTimeWFS;

		public override bool Use()
		{
			base.Use();
			RiseInventoryEvent.Trigger(RiseInventoryEventType.InventoryCloseRequest, null, this.name, null, 0, 0);
			RiseTimeScaleEvent.Trigger(RiseTimeScaleMethods.For, TimeSpeed, Duration, true, 5f, false);
            return true;
		}	
	}
}
