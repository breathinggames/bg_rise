﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;

namespace BreathingGames.Rise
{
	[AddComponentMenu("Rise/Environment/Gravity Point")]
	public class GravityPoint : MonoBehaviour 
	{
		public float DistanceOfEffect;

		protected virtual void OnDrawGizmos()
		{
			Gizmos.color = Color.green;
			Gizmos.DrawWireSphere (this.transform.position, DistanceOfEffect);
		}

	}
}
