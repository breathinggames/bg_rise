﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;

namespace BreathingGames.Rise
{
	/// <summary>
	/// This class describes how achievements are triggered.
	/// It extends the base class AchievementRules
	/// It listens for different event types
	/// </summary>
	public class RiseAchievementRules : Tools.RiseAchievementRules, 
									RiseEventListener<RiseGameEvent>, 
									RiseEventListener<RiseCharacterEvent>, 
									RiseEventListener<RiseEvent>,
									RiseEventListener<RiseStateChangeEvent<RiseCharacterStates.MovementStates>>,
									RiseEventListener<RiseStateChangeEvent<RiseCharacterStates.CharacterConditions>>,
									RiseEventListener<PickableItemEvent>
	{
		/// <summary>
		/// When we catch an GameEvent, we do stuff based on its name
		/// </summary>
		/// <param name="gameEvent">Game event.</param>
		public override void OnRiseEvent(RiseGameEvent gameEvent)
		{

			base.OnRiseEvent (gameEvent);

		}

		public virtual void OnRiseEvent(RiseCharacterEvent characterEvent)
		{
			if (characterEvent.TargetCharacter.CharacterType == RiseCharacter.CharacterTypes.Player)
			{
				switch (characterEvent.EventType)
				{
					case RiseCharacterEventTypes.Jump:
						RiseAchievementManager.AddProgress ("JumpFreak", 1);
						break;
				}	
			}
		}

		public virtual void OnRiseEvent(RiseEvent riseEvent)
		{
			switch (riseEvent.EventType)
			{
				case RiseEventTypes.LevelEnd:
					RiseAchievementManager.UnlockAchievement ("NextExitWillBeTheRightOne");
					break;
				case RiseEventTypes.PlayerDeath:
					RiseAchievementManager.UnlockAchievement ("DeathIsTheBeginning");
					break;
			}
		}

        public virtual void OnRiseEvent(PickableItemEvent pickableItemEvent)
		{
			if (pickableItemEvent.PickedItem != null)
			{
				if (pickableItemEvent.PickedItem.GetComponent<Coin>() != null)
				{
					RiseAchievementManager.AddProgress ("Money", 1);
                }
				if (pickableItemEvent.PickedItem.GetComponent<Stimpack>() != null)
				{
					RiseAchievementManager.UnlockAchievement ("Medic");
				}
                if (pickableItemEvent.PickedItem.GetComponent<StuffedAnimals>() != null)
                {
                    RiseAchievementManager.AddProgress("StuffedAnimals", 1);
                }
			}
        }

		public virtual void OnRiseEvent(RiseStateChangeEvent<RiseCharacterStates.MovementStates> movementEvent)
		{
			/*switch (movementEvent.NewState)
			{

			}*/
		}

		public virtual void OnRiseEvent(RiseStateChangeEvent<RiseCharacterStates.CharacterConditions> conditionEvent)
		{
			/*switch (conditionEvent.NewState)
			{

			}*/
		}

		/// <summary>
		/// On enable, we start listening for GameEvents. You may want to extend that to listen to other types of events.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable ();
			this.RiseEventStartListening<RiseCharacterEvent>();
			this.RiseEventStartListening<RiseEvent>();
			this.RiseEventStartListening<RiseStateChangeEvent<RiseCharacterStates.MovementStates>>();
			this.RiseEventStartListening<RiseStateChangeEvent<RiseCharacterStates.CharacterConditions>>();
			this.RiseEventStartListening<PickableItemEvent>();
		}

		/// <summary>
		/// On disable, we stop listening for GameEvents. You may want to extend that to stop listening to other types of events.
		/// </summary>
		protected override void OnDisable()
		{
			base.OnDisable ();
			this.RiseEventStopListening<RiseCharacterEvent>();
			this.RiseEventStopListening<RiseEvent>();
			this.RiseEventStopListening<RiseStateChangeEvent<RiseCharacterStates.MovementStates>>();
			this.RiseEventStopListening<RiseStateChangeEvent<RiseCharacterStates.CharacterConditions>>();
			this.RiseEventStopListening<PickableItemEvent>();
		}
	}
}