namespace BreathingGames.Rise
{	
	/// <summary>
	/// Interface for player respawn
	/// </summary>
	public interface Respawnable
	{
		void OnPlayerRespawn(CheckPoint checkpoint, RiseCharacter player);
	}
}