using UnityEngine;
using System.Collections;
using BreathingGames.Tools;
using Photon.Pun;

namespace BreathingGames.Rise
{	
	/// <summary>
	/// Add this class to a trigger and it will send your player to the next level
	/// </summary>
	[AddComponentMenu("Rise/Spawn/Finish Level")]
	public class FinishLevel : ButtonActivated 
	{
		public string LevelName;

        private int playerAtTheExit;

        private void Start()
        {
            playerAtTheExit = 0;
        }

        protected override void Update()
        {
            Debug.Log(playerAtTheExit + " / " + PhotonNetwork.PlayerList.Length);

            if (playerAtTheExit == PhotonNetwork.PlayerList.Length)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    GoToNextLevelOnline();
                }                
            }
        }

        /// <summary>
        /// When the button is pressed we start the dialogue
        /// </summary>
        public override void TriggerButtonAction()
		{
			if (!CheckNumberOfUses())
			{
				return;
			}

            /*
			base.TriggerButtonAction ();
			GoToNextLevel();
			ActivateZone ();
            */           

        }			

		/// <summary>
		/// Loads the next level
		/// </summary>
	    public virtual void GoToNextLevel()
	    {
	    	if (LevelManager.Instance!=null)
	    	{
				LevelManager.Instance.GotoLevel(LevelName);
	    	}
	    	else
	    	{
		        LoadingSceneManager.LoadScene(LevelName);
			}
	    }

        /// <summary>
        /// Loads the next level online
        /// </summary>
        public virtual void GoToNextLevelOnline()
        {
            playerAtTheExit = 0;
            PhotonNetwork.LoadLevel(LevelName);
        }

        protected override void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                playerAtTheExit++;
            }
        }

        protected override void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                playerAtTheExit--;
            }
        }
    }
}