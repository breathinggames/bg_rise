﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using BreathingGames.Tools;

namespace BreathingGames.Rise
{	
	/// <summary>
	/// Add this class to a trigger to cause the level to restart when the player hits the trigger
	/// </summary>
	[AddComponentMenu("Rise/Spawn/Level Restarter")]
	public class LevelRestarter : MonoBehaviour 
	{
		/// <summary>
		/// When a character enters the zone, restarts the level
		/// </summary>
		/// <param name="collider">Collider.</param>
	    protected virtual void OnTriggerEnter2D (Collider2D collider)
		{
			RiseCharacter character = collider.GetComponent<RiseCharacter>();

			if (character == null)
			{
				return;
			}
			
			if (character.CharacterType != RiseCharacter.CharacterTypes.Player)
			{
				return;
			}

			LoadingSceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}
	}
}