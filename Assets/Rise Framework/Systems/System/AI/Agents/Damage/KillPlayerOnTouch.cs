using UnityEngine;
using System.Collections;
using BreathingGames.Tools;

namespace BreathingGames.Rise
{	
	/// <summary>
	/// Add this to a GameObject with a Collider2D set to Trigger to have it kill the player on touch.
	/// </summary>
	[AddComponentMenu("Rise/Character/Damage/Kill Player on Touch")] 
	public class KillPlayerOnTouch : MonoBehaviour 
	{
		/// <summary>
		/// When a collision is triggered, check if the thing colliding is actually the player. If yes, kill it.
		/// </summary>
		/// <param name="collider">The object that collides with the KillPlayerOnTouch object.</param>
		protected virtual void OnTriggerEnter2D(Collider2D collider)
		{
			RiseCharacter character = collider.GetComponent<RiseCharacter>();

			if (character == null)
			{
				return;
			}
			
			if (character.CharacterType != RiseCharacter.CharacterTypes.Player)
			{
				return;
			}
			
			LevelManager.Instance.KillPlayer(character);
		}
	}
}