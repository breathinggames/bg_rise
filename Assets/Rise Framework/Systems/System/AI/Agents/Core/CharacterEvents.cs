﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;

namespace BreathingGames.Rise
{	
	/// <summary>
	/// A list of possible events used by the character
	/// </summary>
	public enum RiseCharacterEventTypes
	{
		ButtonActivation,
		Jump
	}

	/// <summary>
	/// CharacterEvents are used in addition to the events triggered by the character's state machine, to signal stuff happening that is not necessarily linked to a change of state
	/// </summary>
	public struct RiseCharacterEvent
	{
		public RiseCharacter TargetCharacter;
		public RiseCharacterEventTypes EventType;
		/// <summary>
		/// Initializes a new instance of the <see cref="BreathingGames.Rise.RiseCharacterEvent"/> struct.
		/// </summary>
		/// <param name="character">Character.</param>
		/// <param name="eventType">Event type.</param>
		public RiseCharacterEvent(RiseCharacter character, RiseCharacterEventTypes eventType)
		{
			TargetCharacter = character;
			EventType = eventType;
		}

        static RiseCharacterEvent e;
        public static void Trigger(RiseCharacter character, RiseCharacterEventTypes eventType)
        {
            e.TargetCharacter = character;
            e.EventType = eventType;
            RiseEventManager.TriggerEvent(e);
        }
    } 

	/// <summary>
	/// An event fired when something takes damage
	/// </summary>
	public struct RiseDamageTakenEvent
	{
		public RiseCharacter AffectedCharacter;
		public GameObject Instigator;
		public float CurrentHealth;
		public float DamageCaused;
		public float PreviousHealth;

		/// <summary>
		/// Initializes a new instance of the <see cref="BreathingGames.Rise.RiseDamageTakenEvent"/> struct.
		/// </summary>
		/// <param name="affectedCharacter">Affected character.</param>
		/// <param name="instigator">Instigator.</param>
		/// <param name="currentHealth">Current health.</param>
		/// <param name="damageCaused">Damage caused.</param>
		/// <param name="previousHealth">Previous health.</param>
		public RiseDamageTakenEvent(RiseCharacter affectedCharacter, GameObject instigator, float currentHealth, float damageCaused, float previousHealth)
		{
			AffectedCharacter = affectedCharacter;
			Instigator = instigator;
			CurrentHealth = currentHealth;
			DamageCaused = damageCaused;
			PreviousHealth = previousHealth;
		}

        static RiseDamageTakenEvent e;
        public static void Trigger(RiseCharacter affectedCharacter, GameObject instigator, float currentHealth, float damageCaused, float previousHealth)
        {
            e.AffectedCharacter = affectedCharacter;
            e.Instigator = instigator;
            e.CurrentHealth = currentHealth;
            e.DamageCaused = damageCaused;
            e.PreviousHealth = previousHealth;
            RiseEventManager.TriggerEvent(e);
        }
    }
}