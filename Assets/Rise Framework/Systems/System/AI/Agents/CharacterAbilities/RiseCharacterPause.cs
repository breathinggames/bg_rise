﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;

namespace BreathingGames.Rise
{
    /// <summary>
    /// Add this component to a character and it'll be able to activate/desactivate the pause
    /// </summary>
    [HiddenProperties("AbilityStopResponses")]
    [AddComponentMenu("Rise/Character/Abilities/Character Pause")] 
	public class RiseCharacterPause : RiseCharacterAbility 
	{
		/// This method is only used to display a helpbox text at the beginning of the ability's inspector
		public override string HelpBoxText() { return "Allows this character (and the player controlling it) to press the pause button to pause the game."; }

		/// <summary>
		/// Every frame, we check the input to see if we need to pause/unpause the game
		/// </summary>
		protected override void HandleInput()
		{
			if (_inputManager.PauseButton.State.CurrentState == RiseInput.ButtonStates.ButtonDown)				
			{
				TriggerPause();
			}
		}

		/// <summary>
		/// If the pause button has been pressed, we change the pause state
		/// </summary>
		protected virtual void TriggerPause()
		{
			if (!AbilityPermitted
			&& (_condition.CurrentState == RiseCharacterStates.CharacterConditions.Normal || _condition.CurrentState == RiseCharacterStates.CharacterConditions.Paused))
			{
				return;
			}
            // we trigger a Pause event for the GameManager and other classes that could be listening to it too
            PlayAbilityStartResponses();
			RiseEvent.Trigger(RiseEventTypes.Pause);
		}

		/// <summary>
		/// Puts the character in the pause state
		/// </summary>
		public virtual void PauseCharacter()
		{
            if (_condition.CurrentState == RiseCharacterStates.CharacterConditions.Normal)
            {
                _condition.ChangeState(RiseCharacterStates.CharacterConditions.Paused);
            }
            else
            {
                _condition.ChangeState(RiseCharacterStates.CharacterConditions.Normal);
            }
            
		}

		/// <summary>
		/// Restores the character to the state it was in before the pause.
		/// </summary>
		public virtual void UnPauseCharacter()
		{
			_condition.RestorePreviousState();
        }
	}
}
