﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using BreathingGames.Tools;

namespace BreathingGames.Rise
{

	[CustomEditor (typeof(RiseCharacterAbility),true)]
	[CanEditMultipleObjects]

	/// <summary>
	/// Adds custom labels to the Character inspector
	/// </summary>

	public class RiseCharacterAbilityInspector : Editor 
	{
        protected SerializedProperty _abilityStartResponses;
        protected SerializedProperty _abilityStopResponses;

        protected List<String> _propertiesToHide;
        protected bool _hasHiddenProperties = false;

        private void OnEnable()
        {
            _propertiesToHide = new List<string>();

            _abilityStartResponses = this.serializedObject.FindProperty("AbilityStartResponses");
            _abilityStopResponses = this.serializedObject.FindProperty("AbilityStopResponses");

            HiddenPropertiesAttribute[] attributes = (HiddenPropertiesAttribute[])target.GetType().GetCustomAttributes(typeof(HiddenPropertiesAttribute), false);
            if (attributes != null)
            {
                if (attributes.Length != 0)
                {
                    if (attributes[0].PropertiesNames != null)
                    {
                        _propertiesToHide = new List<String>(attributes[0].PropertiesNames);                        
                        _hasHiddenProperties = true;
                    }
                }                
            }
        }
        
		/// <summary>
		/// When inspecting a Character, adds to the regular inspector some labels, useful for debugging
		/// </summary>
		public override void OnInspectorGUI()
		{
			RiseCharacterAbility t = (target as RiseCharacterAbility);

			serializedObject.Update();
            EditorGUI.BeginChangeCheck();

            if (t.HelpBoxText() != "")
			{
				EditorGUILayout.HelpBox(t.HelpBoxText(),MessageType.Info);
			}

			Editor.DrawPropertiesExcluding(serializedObject, new string[] { "AbilityStartResponses", "AbilityStopResponses" });

			EditorGUILayout.Space();
                        
            if (_propertiesToHide.Count > 0)
            {
                if (_propertiesToHide.Count < 2)
                {
                    EditorGUILayout.LabelField("Responses", EditorStyles.boldLabel);
                }                
                if (!_propertiesToHide.Contains("AbilityStartResponses"))
                {
                    EditorGUILayout.PropertyField(_abilityStartResponses);
                }
                if (!_propertiesToHide.Contains("AbilityStopResponses"))
                {
                    EditorGUILayout.PropertyField(_abilityStopResponses);
                }
            }
            else
            {
                EditorGUILayout.LabelField("Responses", EditorStyles.boldLabel);
                EditorGUILayout.PropertyField(_abilityStartResponses);
                EditorGUILayout.PropertyField(_abilityStopResponses);
            }

            if (EditorGUI.EndChangeCheck())
            {
                serializedObject.ApplyModifiedProperties();
            }                
        }	
	}
}