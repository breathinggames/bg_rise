using UnityEngine;
using System.Collections;
using BreathingGames.Tools;

namespace BreathingGames.Rise
{	
	/// <summary>
	/// Add this class to a character so it can use weapons
	/// Note that this component will trigger animations (if their parameter is present in the Animator), based on 
	/// the current weapon's Animations
	/// Animator parameters : defined from the Weapon's inspector
	/// </summary>
	[AddComponentMenu("Rise/Character/Abilities/Character Handle Secondary Weapon")] 
	public class CharacterHandleSecondaryWeapon : CharacterHandleWeapon 
	{
		/// <summary>
		/// Gets input and triggers methods based on what's been pressed
		/// </summary>
		protected override void HandleInput ()
		{			

			if ((_inputManager.SecondaryShootButton.State.CurrentState == RiseInput.ButtonStates.ButtonDown) || (ContinuousPress && (CurrentWeapon.TriggerMode == Weapon.TriggerModes.Auto) && (_inputManager.SecondaryShootButton.State.CurrentState == RiseInput.ButtonStates.ButtonPressed)))
			{
				ShootStart();
			}

			if ((_inputManager.SecondaryShootAxis == RiseInput.ButtonStates.ButtonDown) || (ContinuousPress && (CurrentWeapon.TriggerMode == Weapon.TriggerModes.Auto) && (_inputManager.SecondaryShootAxis == RiseInput.ButtonStates.ButtonPressed)))
			{
				ShootStart();
			}

			if (_inputManager.ReloadButton.State.CurrentState == RiseInput.ButtonStates.ButtonDown)
			{
				Reload();
            }

            if ((_inputManager.SecondaryShootButton.State.CurrentState == RiseInput.ButtonStates.ButtonUp) || (_inputManager.SecondaryShootAxis == RiseInput.ButtonStates.ButtonUp))
            {
                ShootStop();
            }

            if (CurrentWeapon != null)
            {
                if ((CurrentWeapon.WeaponState.CurrentState == Weapon.WeaponStates.WeaponDelayBetweenUses)
                && ((_inputManager.SecondaryShootAxis == RiseInput.ButtonStates.Off) && (_inputManager.SecondaryShootButton.State.CurrentState == RiseInput.ButtonStates.Off)))
                {
                    ShootStop();
                }
            }            
        }				
    }
}