﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;

namespace BreathingGames.Rise
{
    [HiddenProperties("AbilityStartResponses", "AbilityStopResponses")]
    [AddComponentMenu("Rise/Character/Abilities/Character Particles")] 
	/// <summary>
	/// Add this component to a Character and bind ParticleSystems to it to have it emit particles when certain states are active.
	/// You can have a look at the RetroCorgi demo character for examples of how to use it.
	/// </summary>
	public class RiseCharacterParticles : RiseCharacterAbility 
	{
		[Header("Character Particles")]
		/// the particle system to use when the character is idle
		public ParticleSystem IdleParticles;
		/// the particle system to use when the character is walking
		public ParticleSystem WalkingParticles;
		/// the particle system to use when the character is crouching
		public ParticleSystem CrouchingParticles;
		/// the particle system to use when the character is crawling
		public ParticleSystem CrawlingParticles;
		/// the particle system to use when the character is dangling
		public ParticleSystem DanglingParticles;
		/// the particle system to use when the character is dashing
		public ParticleSystem DashingParticles;
		/// the particle system to use when the character is diving
		public ParticleSystem DivingParticles;
		/// the particle system to use when the character is gripping
		public ParticleSystem GrippingParticles;
		/// the particle system to use when the character is jetpacking
		public ParticleSystem JetpackingParticles;
		/// the particle system to use when the character is jumping
		public ParticleSystem JumpingParticles;
		/// the particle system to use when the character is on a ladder
		public ParticleSystem LadderParticles;
		/// the particle system to use when the character is looking up
		public ParticleSystem LookupParticles;
		/// the particle system to use when the character is pushing
		public ParticleSystem PushParticles;
		/// the particle system to use when the character is running
		public ParticleSystem RunParticles;
		/// the particle system to use when the character is wallclinging
		public ParticleSystem WallclingingParticles;
		/// the particle system to use when the character is walljumping
		public ParticleSystem WalljumpParticles;

		protected ParticleSystem.EmissionModule _emissionModule;
		protected RiseCharacterStates.MovementStates _stateLastFrame;

		/// <summary>
		/// On update we go through all our particle systems and activate them if needed
		/// </summary>
		public override void ProcessAbility()
		{
			base.ProcessAbility ();
			HandleParticleSystem (IdleParticles, RiseCharacterStates.MovementStates.Idle);
			HandleParticleSystem (WalkingParticles, RiseCharacterStates.MovementStates.Walking);
			HandleParticleSystem (CrouchingParticles, RiseCharacterStates.MovementStates.Crouching);
			HandleParticleSystem (CrawlingParticles, RiseCharacterStates.MovementStates.Crawling);
			HandleParticleSystem (DanglingParticles, RiseCharacterStates.MovementStates.Dangling);
			HandleParticleSystem (DashingParticles, RiseCharacterStates.MovementStates.Dashing);
			HandleParticleSystem (DivingParticles, RiseCharacterStates.MovementStates.Diving);
			HandleParticleSystem (GrippingParticles, RiseCharacterStates.MovementStates.Gripping);
			HandleParticleSystem (JetpackingParticles, RiseCharacterStates.MovementStates.Jetpacking);
			HandleParticleSystem (JumpingParticles, RiseCharacterStates.MovementStates.Jumping);
			HandleParticleSystem (LadderParticles, RiseCharacterStates.MovementStates.LadderClimbing);
			HandleParticleSystem (LookupParticles, RiseCharacterStates.MovementStates.LookingUp);
			HandleParticleSystem (PushParticles, RiseCharacterStates.MovementStates.Pushing);
			HandleParticleSystem (RunParticles, RiseCharacterStates.MovementStates.Running);
			HandleParticleSystem (WallclingingParticles, RiseCharacterStates.MovementStates.WallClinging);
			HandleParticleSystem (WalljumpParticles, RiseCharacterStates.MovementStates.WallJumping);
			_stateLastFrame = _movement.CurrentState;
		}

		/// <summary>
		/// Checks if the specified state is active, and if yes, triggers the particle system's emission
		/// </summary>
		/// <param name="system">System.</param>
		/// <param name="state">State.</param>
		protected virtual void HandleParticleSystem(ParticleSystem system, RiseCharacterStates.MovementStates state)
		{
			if (system == null)
			{
				return;
			}
			if (_movement.CurrentState == state)
			{
				if (!system.main.loop && _stateLastFrame != state)
				{
					system.Clear ();
					system.Play ();
				}
				_emissionModule = system.emission;
				_emissionModule.enabled = true;
			} 
			else
			{
				_emissionModule = system.emission;
				_emissionModule.enabled = false;
			}
		}
	}
}