﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using BreathingGames.Tools;
using UnityEngine.SceneManagement;

namespace BreathingGames.Tools
{	
	/// <summary>
	/// Add this component on an object, specify a scene name in its inspector, and call LoadScene() to load the desired scene.
	/// </summary>
	public class RiseLoadScene : MonoBehaviour 
	{
		/// the possible modes to load scenes.
		public enum LoadingSceneModes { UnityNative, LoadingSceneManager }

		/// the name of the scene that needs to be loaded when LoadScene gets called
		public string SceneName;

		public LoadingSceneModes LoadingSceneMode = LoadingSceneModes.UnityNative;

		/// <summary>
		/// Loads the scene specified in the inspector
		/// </summary>
		public virtual void LoadScene()
		{
			if (LoadingSceneMode == LoadingSceneModes.UnityNative)
			{
				SceneManager.LoadScene (SceneName);
			}
			if (LoadingSceneMode == LoadingSceneModes.LoadingSceneManager)
			{
				LoadingSceneManager.LoadScene (SceneName); 
			}
		}
	}
}
