﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BreathingGames.Tools
{
    public class RiseObjectPool : MonoBehaviour
    {
        [ReadOnly]
        public List<GameObject> PooledGameObjects;
    }
}
