﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using BreathingGames.Tools;
using UnityEngine.Rendering;

namespace BreathingGames.TopDownEngine
{
    [CustomEditor(typeof(Playlist))]
    [CanEditMultipleObjects]

    /// <summary>
    /// A custom editor that displays the current state of a playlist when the game is running
    /// </summary>
    public class PlaylistEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            Playlist playlist = (Playlist)target;

            DrawDefaultInspector();

            if (playlist.PlaylistState != null)
            {
                EditorGUILayout.LabelField("Current State", playlist.PlaylistState.CurrentState.ToString());
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}