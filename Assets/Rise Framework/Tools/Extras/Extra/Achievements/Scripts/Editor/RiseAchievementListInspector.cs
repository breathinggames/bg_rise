﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine.UI;

namespace BreathingGames.Tools
{
	[CustomEditor(typeof(RiseAchievementList),true)]
	/// <summary>
	/// Custom inspector for the MMAchievementList scriptable object. 
	/// </summary>
	public class RiseAchievementListInspector : Editor 
	{
		/// <summary>
		/// When drawing the GUI, adds a "Reset Achievements" button, that does exactly what you think it does.
		/// </summary>
		public override void OnInspectorGUI()
		{
			DrawDefaultInspector ();
			RiseAchievementList achievementList = (RiseAchievementList)target;
			if(GUILayout.Button("Reset Achievements"))
			{
				achievementList.ResetAchievements();
			}	
			EditorUtility.SetDirty (achievementList);
		}
	}
}