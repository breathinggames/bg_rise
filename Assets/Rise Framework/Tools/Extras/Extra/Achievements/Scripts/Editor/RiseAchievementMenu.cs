﻿using UnityEngine;
using System.Collections;
using BreathingGames.Tools;
using UnityEditor;

namespace BreathingGames.Tools
{	
	public static class RiseAchievementMenu 
	{
		[MenuItem("Tools/Rise/Reset all achievements", false,21)]
		/// <summary>
		/// Adds a menu item to enable help
		/// </summary>
		private static void EnableHelpInInspectors()
		{
			RiseAchievementManager.ResetAllAchievements ();
		}
	}
}