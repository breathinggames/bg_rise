﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BreathingGames.Tools
{
	/// <summary>
	/// An event type used to broadcast the fact that an achievement has been unlocked
	/// </summary>
	public struct RiseAchievementUnlockedEvent
	{
		/// the achievement that has been unlocked
		public RiseAchievement Achievement;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="newAchievement">New achievement.</param>
		public RiseAchievementUnlockedEvent(RiseAchievement newAchievement)
		{
			Achievement = newAchievement;
        }

        static RiseAchievementUnlockedEvent e;
        public static void Trigger(RiseAchievement newAchievement)
        {
            e.Achievement = newAchievement;
            RiseEventManager.TriggerEvent(e);
        }
    }
}