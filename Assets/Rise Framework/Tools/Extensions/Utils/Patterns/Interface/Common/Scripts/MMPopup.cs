﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using BreathingGames.Tools;

namespace BreathingGames.RiseInterface
{	
	/// <summary>
	/// A component to handle popups, their opening and closing
	/// </summary>
	public class MMPopup : MonoBehaviour 
	{
		/// true if the popup is currently open
		public bool CurrentlyOpen = false;

        [Header("Fader")]
        public RiseTween.TweenCurve FadeCurve = RiseTween.TweenCurve.LinearTween;
		public float FaderOpenDuration = 0.2f;
		public float FaderCloseDuration = 0.2f;
		public float FaderOpacity = 0.8f;

		protected Animator _animator;

		/// <summary>
		/// On Start, we initialize our popup
		/// </summary>
		protected virtual void Start()
		{
			Initialization ();
		}

		/// <summary>
		/// On Init, we grab our animator and store it for future use
		/// </summary>
		protected virtual void Initialization()
		{
			_animator = GetComponent<Animator> ();
		}

		/// <summary>
		/// On update, we update our animator parameter
		/// </summary>
		protected virtual void Update()
		{
			if (_animator != null)
			{
				_animator.SetBool ("Closed", !CurrentlyOpen);
			}
		}

		/// <summary>
		/// Opens the popup
		/// </summary>
		public virtual void Open()
		{
			if (CurrentlyOpen)
			{
				return;
			}

			MMFadeEvent.Trigger(FaderOpenDuration, FaderOpacity, FadeCurve, 0, true);
			_animator.SetTrigger ("Open");
			CurrentlyOpen = true;
		}

		/// <summary>
		/// Closes the popup
		/// </summary>
		public virtual void Close()
		{
			if (!CurrentlyOpen)
			{
				return;
			}

			MMFadeEvent.Trigger(FaderCloseDuration, 0f, FadeCurve, 0, true);
			_animator.SetTrigger ("Close");
			CurrentlyOpen = false;
		}

	}
}
