﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace BreathingGames.Tools
{
    /// <summary>
    /// Game object extensions
    /// </summary>
    public static class GameObjectExtensions
    {
        static List<Component> rise_ComponentCache = new List<Component>();

        /// <summary>
        /// Grabs a component without allocating memory uselessly
        /// </summary>
        /// <param name="this"></param>
        /// <param name="componentType"></param>
        /// <returns></returns>
		public static Component RiseGetComponentNoAlloc(this GameObject @this, System.Type componentType)
        {
            @this.GetComponents(componentType, rise_ComponentCache);
            var component = rise_ComponentCache.Count > 0 ? rise_ComponentCache[0] : null;
            rise_ComponentCache.Clear();
            return component;
        }

        /// <summary>
        /// Grabs a component without allocating memory uselessly
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this"></param>
        /// <returns></returns>
        public static T RiseGetComponentNoAlloc<T>(this GameObject @this) where T : Component
        {
            @this.GetComponents(typeof(T), rise_ComponentCache);
            var component = rise_ComponentCache.Count > 0 ? rise_ComponentCache[0] : null;
            rise_ComponentCache.Clear();
            return component as T;
        }
    }
}
