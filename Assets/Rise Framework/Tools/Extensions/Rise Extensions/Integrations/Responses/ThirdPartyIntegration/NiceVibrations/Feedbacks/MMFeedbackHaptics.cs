﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BreathingGames.Response;
#if NICEVIBRATIONS_INSTALLED
using MoreMountains.NiceVibrations;
#endif

namespace BreathingGames.ThirdPartyIntegration
{
    [AddComponentMenu("")]
    [ResponsePathAttribute("Haptics")]
    [ResponseHelpAttribute("This feedback lets you trigger haptic feedbacks through the Nice Vibrations asset, available on the Unity Asset Store. You'll need to own that asset and have it " +
        "in your project for this to work.")]
    public class MMFeedbackHaptics : RiseResponse
    {
#if NICEVIBRATIONS_INSTALLED
        [Header("Haptics")]
        public HapticTypes HapticType = HapticTypes.None;
#endif

        protected override void CustomPlayResponse(Vector3 position, float attenuation = 1.0f)
        {
            if (Active)
            {
#if NICEVIBRATIONS_INSTALLED
                MMVibrationManager.Haptic(HapticType);
#endif
            }
        }
    }
}
