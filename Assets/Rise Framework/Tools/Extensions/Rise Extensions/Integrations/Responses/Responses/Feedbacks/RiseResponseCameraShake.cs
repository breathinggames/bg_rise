﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BreathingGames.Response
{
    /// <summary>
    /// This response will send a shake event when played
    /// </summary>
    [AddComponentMenu("")]
    [ResponseHelpAttribute("Define camera shake properties (duration in seconds, amplitude and frequency), and this will broadcast a CameraShakeEvent with these same settings. You'll need to add a CinemachineCameraShaker on your camera for this to work (or a CinemachineZoom component if you're using Cinemachine). Note that although this event and system was built for cameras in mind, you could technically use it to shake other objects as well.")]
    [ResponsePathAttribute("Camera/Camera Shake")]
    public class RiseResponseCameraShake : RiseResponse
    {
        [Header("Camera Shake")]
        public bool RepeatUntilStopped = false;
        public int Channel = 0;
        /// the properties of the shake (duration, intensity, frequenc)
        public CameraShakeProperties CameraShakeProperties = new CameraShakeProperties(0.1f, 0.2f, 40f);

        /// <summary>
        /// On Play, sends a shake camera event
        /// </summary>
        /// <param name="position"></param>
        /// <param name="attenuation"></param>
        protected override void CustomPlayResponse(Vector3 position, float attenuation = 1.0f)
        {
            if (Active)
            {
                CameraShakeEvent.Trigger(CameraShakeProperties.Duration, CameraShakeProperties.Amplitude * attenuation, CameraShakeProperties.Frequency, RepeatUntilStopped, Channel);
            }
        }

        protected override void CustomStopResponse(Vector3 position, float attenuation = 1)
        {
            base.CustomStopResponse(position, attenuation);
            if (Active)
            {
                CameraShakeStopEvent.Trigger(Channel);
            }
        }
    }
}
