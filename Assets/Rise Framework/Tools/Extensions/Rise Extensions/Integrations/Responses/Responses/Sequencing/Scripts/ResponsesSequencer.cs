﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BreathingGames.Response
{
    /// <summary>
    /// A Sequencer with ready made slots to play responses
    /// </summary>
    public class ResponsesSequencer : Sequencer
    {
        /// the list of audio clips to play (one per track)
        public List<RiseResponses> Responses;

        /// <summary>
        /// On beat we play our audio sources
        /// </summary>
        protected override void OnBeat()
        {
            base.OnBeat();
            for (int i = 0; i < Sequence.SequenceTracks.Count; i++)
            {
                if ((Sequence.SequenceTracks[i].Active) && (Sequence.QuantizedSequence[i].Line[CurrentSequenceIndex].ID != -1))
                {
                    if ((Responses.Count > i) && (Responses[i] != null))
                    {
                        Responses[i].PlayResponses();
                    }
                }
            }
        }

        /// <summary>
        /// When playing our event for control, we play our audiosource
        /// </summary>
        /// <param name="index"></param>
        public override void PlayTrackEvent(int index)
        {
            if (!Application.isPlaying)
            {
                return;
            }
            base.PlayTrackEvent(index);
            Responses[index].PlayResponses();
        }

        /// <summary>
        /// When looking for changes we make sure we have enough sounds in our array
        /// </summary>
        public override void EditorMaintenance()
        {
            base.EditorMaintenance();
            SetupResponses();
        }

        /// <summary>
        /// Ensures the array is always the right length
        /// </summary>
        public virtual void SetupResponses()
        {
            if (Sequence == null)
            {
                return;
            }
            // setup events
            if (Responses.Count < Sequence.SequenceTracks.Count)
            {
                for (int i = 0; i < Sequence.SequenceTracks.Count; i++)
                {
                    if (i >= Responses.Count)
                    {
                        Responses.Add(null);
                    }
                }
            }
            if (Responses.Count > Sequence.SequenceTracks.Count)
            {
                Responses.Clear();
                for (int i = 0; i < Sequence.SequenceTracks.Count; i++)
                {
                    Responses.Add(null);
                }
            }
        }
    }
}
