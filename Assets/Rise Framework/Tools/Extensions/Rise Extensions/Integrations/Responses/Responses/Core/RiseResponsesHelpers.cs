﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEditor;
using System.Reflection;

namespace BreathingGames.Response
{
    [AddComponentMenu("")]
    public class RiseResponsesHelpers : MonoBehaviour
    {
        /// <summary>
		/// Remaps a value x in interval [A,B], to the proportional value in interval [C,D]
		/// </summary>
		/// <param name="x">The value to remap.</param>
		/// <param name="A">the minimum bound of interval [A,B] that contains the x value</param>
		/// <param name="B">the maximum bound of interval [A,B] that contains the x value</param>
		/// <param name="C">the minimum bound of target interval [C,D]</param>
		/// <param name="D">the maximum bound of target interval [C,D]</param>
		public static float Remap(float x, float A, float B, float C, float D)
        {
            float remappedValue = C + (x - A) / (B - A) * (D - C);
            return remappedValue;
        }
    }

    public class RiseFReadOnlyAttributeAttribute : PropertyAttribute { }

    [System.AttributeUsage(System.AttributeTargets.Field)]
    public class RiseFInspectorButtonAttributeAttribute : PropertyAttribute
    {
        public readonly string MethodName;

        public RiseFInspectorButtonAttributeAttribute(string MethodName)
        {
            this.MethodName = MethodName;
        }
    }

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class | AttributeTargets.Struct, Inherited = true)]
    public class RiseFEnumConditionAttributeAttribute : PropertyAttribute
    {
        public string ConditionEnum = "";
        public bool Hidden = false;

        BitArray bitArray = new BitArray(32);
        public bool ContainsBitFlag(int enumValue)
        {
            return bitArray.Get(enumValue);
        }

        public RiseFEnumConditionAttributeAttribute(string conditionBoolean, params int[] enumValues)
        {
            this.ConditionEnum = conditionBoolean;
            this.Hidden = true;

            for (int i = 0; i < enumValues.Length; i++)
            {
                bitArray.Set(enumValues[i], true);
            }
        }
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(RiseFInspectorButtonAttributeAttribute))]
    public class RiseFInspectorButtonPropertyDrawer : PropertyDrawer
    {
        private MethodInfo _eventMethodInfo = null;

        public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
        {
            RiseFInspectorButtonAttributeAttribute inspectorButtonAttribute = (RiseFInspectorButtonAttributeAttribute)attribute;

            float buttonLength = position.width;
            Rect buttonRect = new Rect(position.x + (position.width - buttonLength) * 0.5f, position.y, buttonLength, position.height);

            if (GUI.Button(buttonRect, inspectorButtonAttribute.MethodName))
            {
                System.Type eventOwnerType = prop.serializedObject.targetObject.GetType();
                string eventName = inspectorButtonAttribute.MethodName;

                if (_eventMethodInfo == null)
                {
                    _eventMethodInfo = eventOwnerType.GetMethod(eventName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
                }

                if (_eventMethodInfo != null)
                {
                    _eventMethodInfo.Invoke(prop.serializedObject.targetObject, null);
                }
                else
                {
                    Debug.LogWarning(string.Format("InspectorButton: Unable to find method {0} in {1}", eventName, eventOwnerType));
                }
            }
        }
    }
#endif

    public class RiseFInformationAttributeAttributeAttribute : PropertyAttribute
    {
        public enum InformationType { Error, Info, None, Warning }

#if UNITY_EDITOR
        public string Message;
        public MessageType Type;
        public bool MessageAfterProperty;

        public RiseFInformationAttributeAttributeAttribute(string message, InformationType type, bool messageAfterProperty)
        {
            this.Message = message;
            if (type == InformationType.Error) { this.Type = UnityEditor.MessageType.Error; }
            if (type == InformationType.Info) { this.Type = UnityEditor.MessageType.Info; }
            if (type == InformationType.Warning) { this.Type = UnityEditor.MessageType.Warning; }
            if (type == InformationType.None) { this.Type = UnityEditor.MessageType.None; }
            this.MessageAfterProperty = messageAfterProperty;
        }
#else
		public RiseFInformationAttributeAttributeAttribute(string message, InformationType type, bool messageAfterProperty)
		{

		}
#endif
    }

    public class RiseFHiddenAttributeAttribute : PropertyAttribute { }

    [AttributeUsage(System.AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class | AttributeTargets.Struct, Inherited = true)]
    public class RiseFConditionAttributeAttribute : PropertyAttribute
    {
        public string ConditionBoolean = "";
        public bool Hidden = false;

        public RiseFConditionAttributeAttribute(string conditionBoolean)
        {
            this.ConditionBoolean = conditionBoolean;
            this.Hidden = false;
        }

        public RiseFConditionAttributeAttribute(string conditionBoolean, bool hideInInspector)
        {
            this.ConditionBoolean = conditionBoolean;
            this.Hidden = hideInInspector;
        }
    }

    public static class RiseResponseStaticMethods
    {
        static List<Component> m_ComponentCache = new List<Component>();

        /// <summary>
        /// Grabs a component without allocating memory uselessly
        /// </summary>
        /// <param name="this"></param>
        /// <param name="componentType"></param>
        /// <returns></returns>
		public static Component GetComponentNoAlloc(this GameObject @this, System.Type componentType)
        {
            @this.GetComponents(componentType, m_ComponentCache);
            var component = m_ComponentCache.Count > 0 ? m_ComponentCache[0] : null;
            m_ComponentCache.Clear();
            return component;
        }

        /// <summary>
        /// Grabs a component without allocating memory uselessly
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this"></param>
        /// <returns></returns>
        public static T RiseFGetComponentNoAlloc<T>(this GameObject @this) where T : Component
        {
            @this.GetComponents(typeof(T), m_ComponentCache);
            var component = m_ComponentCache.Count > 0 ? m_ComponentCache[0] : null;
            m_ComponentCache.Clear();
            return component as T;
        }
    }

    /// <summary>
    /// Atttribute used to mark response class.
    /// The provided path is used to sort the response list displayed in the response manager dropdown
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ResponsePathAttributeAttribute : System.Attribute
    {
        public string Path;
        public string Name;

        public ResponsePathAttributeAttribute(string path)
        {
            Path = path;
            Name = path.Split('/').Last();
        }

        static public string GetResponseDefaultName(System.Type type)
        {
            var attribute = type.GetCustomAttributes(false).OfType<ResponsePathAttributeAttribute>().FirstOrDefault();
            return attribute != null ? attribute.Name : type.Name;
        }

        static public string GetResponseDefaultPath(System.Type type)
        {
            var attribute = type.GetCustomAttributes(false).OfType<ResponsePathAttributeAttribute>().FirstOrDefault();
            return attribute != null ? attribute.Path : type.Name;
        }
    }

    /// <summary>
    /// Atttribute used to mark response class.
    /// The contents allow you to specify a help text for each response
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ResponseHelpAttributeAttribute : System.Attribute
    {
        public string HelpText;

        public ResponseHelpAttributeAttribute(string helpText)
        {
            HelpText = helpText;
        }

        static public string GetResponseHelpText(System.Type type)
        {
            var attribute = type.GetCustomAttributes(false).OfType<ResponseHelpAttributeAttribute>().FirstOrDefault();
            return attribute != null ? attribute.HelpText : "";
        }
    }
}