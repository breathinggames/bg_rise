﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BreathingGames.Response
{
    /// <summary>
    /// A class collecting delay, cooldown and repeat values, to be used to define the behaviour of each Response
    /// </summary>
    [System.Serializable]
    public class RiseResponseTiming
    {
        [Header("Delays")]
        /// the initial delay to apply before playing the delay (in seconds)
        public float InitialDelay = 0f;
        /// the cooldown duration mandatory between two plays
        public float CooldownDuration = 0f;

        [Header("Repeat")]
        /// the repeat mode, whether the response should be played once, multiple times, or forever
        public int NumberOfRepeats = 0;
        /// if this is true, the response will be repeated forever
        public bool RepeatForever = false;
        /// the delay (in seconds) between repeats
        public float DelayBetweenRepeats = 1f;

        [Header("Sequence")]
        public Sequence Sequence;
        public int TrackID = 0;
        public bool Quantized = false;
        [RiseFConditionAttribute("Quantized", true)]
        public int TargetBPM = 120;
    }
}
