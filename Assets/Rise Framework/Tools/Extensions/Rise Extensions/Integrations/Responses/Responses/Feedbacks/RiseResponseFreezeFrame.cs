﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BreathingGames.Response
{
    /// <summary>
    /// This response will trigger a freeze frame event when played, pausing the game for the specified duration (usually short, but not necessarily)
    /// </summary>
    [AddComponentMenu("")]
    [ResponseHelpAttribute("This response will freeze the timescale for the specified duration (in seconds). I usually go with 0.01s or 0.02s, but feel free to tweak it to your liking. It requires a TimeManager in your scene to work.")]
    [ResponsePathAttribute("Time/Freeze Frame")]
    public class RiseResponseFreezeFrame : RiseResponse
    {
        [Header("Freeze Frame")]
        /// the duration of the freeze frame
        public float FreezeFrameDuration = 0.02f;

        /// <summary>
        /// On Play we trigger a freeze frame event
        /// </summary>
        /// <param name="position"></param>
        /// <param name="attenuation"></param>
        protected override void CustomPlayResponse(Vector3 position, float attenuation = 1.0f)
        {
            if (Active)
            {
                RiseFreezeFrameEvent.Trigger(FreezeFrameDuration);
            }
        }
    }
}
