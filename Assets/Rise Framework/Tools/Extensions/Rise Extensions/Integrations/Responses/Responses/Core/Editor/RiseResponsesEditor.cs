﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace BreathingGames.Response
{
    /// <summary>
    /// A custom editor displaying a foldable list of Responses, a dropdown to add more, as well as test buttons to test your Responses at runtime
    /// </summary>
    [CustomEditor(typeof(RiseResponses))]
    public class RiseResponsesEditor : Editor
    {
        /// <summary>
        /// A helper class to copy and paste Responses properties
        /// </summary>
        static class ResponseCopy
        {
            static public System.Type Type { get; private set; }
            static List<SerializedProperty> Properties = new List<SerializedProperty>();

            static string[] IgnoreList = new string[]
            {
            "m_ObjectHideFlags",
            "m_CorrespondingSourceObject",
            "m_PrefabInstance",
            "m_PrefabAsset",
            "m_GameObject",
            "m_Enabled",
            "m_EditorHideFlags",
            "m_Script",
            "m_Name",
            "m_EditorClassIdentifier"
            };

            static public void Copy(SerializedObject serializedObject)
            {
                Type = serializedObject.targetObject.GetType();
                Properties.Clear();

                SerializedProperty property = serializedObject.GetIterator();
                property.Next(true);
                do
                {
                    if (!IgnoreList.Contains(property.name))
                    {
                        Properties.Add(property.Copy());
                    }
                } while (property.Next(false));
            }

            static public void Paste(SerializedObject target)
            {
                if (target.targetObject.GetType() == Type)
                {
                    for (int i = 0; i < Properties.Count; i++)
                    {
                        target.CopyFromSerializedProperty(Properties[i]);
                    }
                }
            }

            static public bool HasCopy()
            {
                return Properties != null && Properties.Count > 0;
            }
        }

        SerializedProperty _riseResponses;
        SerializedProperty _riseResponsesInitializationMode;
        Dictionary<RiseResponse, Editor> _editors;

        List<System.Type> _types;
        string[] _typeDisplays;
        int _draggedStartID = -1;
        int _draggedEndID = -1;

        static bool _debugView = false;

        /// <summary>
        /// On Enable, grabs properties and initializes the add Responses dropdown's contents
        /// </summary>
        void OnEnable()
        {
            // Get properties
            _riseResponses = serializedObject.FindProperty("Responses");
            _riseResponsesInitializationMode = serializedObject.FindProperty("InitializationMode");

            // Create editors
            _editors = new Dictionary<RiseResponse, Editor>();
            for (int i = 0; i < _riseResponses.arraySize; i++)
                AddEditor(_riseResponses.GetArrayElementAtIndex(i).objectReferenceValue as RiseResponse);

            // Retrieve available Responses
            _types = (from domainAssembly in System.AppDomain.CurrentDomain.GetAssemblies()
                     from assemblyType in domainAssembly.GetTypes()
                     where assemblyType.IsSubclassOf(typeof(RiseResponse))
                     select assemblyType).ToList();

            // Create display list from types
            List<string> typeNames = new List<string>();
            typeNames.Add("Add new response...");
            for (int i = 0; i < _types.Count; i++)
            {
                typeNames.Add(ResponsePathAttributeAttribute.GetResponseDefaultPath(_types[i]));
            }

            _typeDisplays = typeNames.ToArray();
        }

        /// <summary>
        /// Draws the inspector, complete with helpbox, init mode selection, list of Responses, Responses selection and test buttons 
        /// </summary>
        public override void OnInspectorGUI()
        {
            var e = Event.current;

            // Update object

            serializedObject.Update();

            Undo.RecordObject(target, "Modified Response Manager");

            EditorGUILayout.Space();


            EditorGUILayout.HelpBox("Select responses from the 'add a response' dropdown and customize them. Remember, if you don't use auto initialization (Awake or Start), " +
                                    "you'll need to initialize them via script.", MessageType.None);

            // Initialisation

            RiseResponseStyling.DrawSection("Initialization");

            EditorGUILayout.PropertyField(_riseResponsesInitializationMode);

            // Draw list

            RiseResponseStyling.DrawSection("Responses");

            for (int i = 0; i < _riseResponses.arraySize; i++)
            {
                RiseResponseStyling.DrawSplitter();

                SerializedProperty property = _riseResponses.GetArrayElementAtIndex(i);

                // Failsafe but should not happend

                if (property.objectReferenceValue == null)
                    continue;

                // Retrieve response

                RiseResponse response = property.objectReferenceValue as RiseResponse;
                response.hideFlags = _debugView ? HideFlags.None : HideFlags.HideInInspector;

                Undo.RecordObject(response, "Modified Response");

                // Draw header

                int id = i;
                bool isExpanded = property.isExpanded;
                Rect headerRect = RiseResponseStyling.DrawHeader(
                        ref isExpanded,
                        ref response.Active,
                        response.Label,
                        (GenericMenu menu) =>
                        {
                            if (Application.isPlaying)
                                menu.AddItem(new GUIContent("Play"), false, () => PlayResponse(id));
                            else
                                menu.AddDisabledItem(new GUIContent("Play"));
                            menu.AddSeparator(null);
                            //menu.AddItem(new GUIContent("Reset"), false, () => ResetFeedback(id));
                            menu.AddItem(new GUIContent("Remove"), false, () => RemoveResponse(id));
                            menu.AddSeparator(null);
                            menu.AddItem(new GUIContent("Copy"), false, () => CopyResponse(id));
                            if (ResponseCopy.HasCopy() && ResponseCopy.Type == response.GetType())
                                menu.AddItem(new GUIContent("Paste"), false, () => PasteResponse(id));
                            else
                                menu.AddDisabledItem(new GUIContent("Paste"));
                        });

                // Check if we start dragging this response

                switch (e.type)
                {
                    case EventType.MouseDown:
                        if (headerRect.Contains(e.mousePosition))
                        {
                            _draggedStartID = i;
                            e.Use();
                        }
                        break;
                    default:
                        break;
                }

                // Draw blue rect if response is being dragged

                if (_draggedStartID == i && headerRect != Rect.zero)
                {
                    Color color = new Color(0, 1, 1, 0.2f);
                    EditorGUI.DrawRect(headerRect, color);
                }

                // If hovering at the top of the response while dragging one, check where the response should be dropped : top or bottom

                if (headerRect.Contains(e.mousePosition))
                {
                    if (_draggedStartID >= 0)
                    {
                        _draggedEndID = i;

                        Rect headerSplit = headerRect;
                        headerSplit.height *= 0.5f;
                        headerSplit.y += headerSplit.height;
                        if (headerSplit.Contains(e.mousePosition))
                            _draggedEndID = i + 1;
                    }
                }

                // If expanded, draw response editor

                property.isExpanded = isExpanded;
                if (isExpanded)
                {
                    EditorGUI.BeginDisabledGroup(!response.Active);

                    string helpText = ResponseHelpAttributeAttribute.GetResponseHelpText(response.GetType());
                    if (helpText != "")
                    {
                        GUIStyle style = new GUIStyle(EditorStyles.helpBox);
                        style.richText = true;
                        float newHeight = style.CalcHeight(new GUIContent(helpText), EditorGUIUtility.currentViewWidth);
                        EditorGUILayout.LabelField(helpText, style);
                    }                    

                    EditorGUILayout.Space();

                    if (!_editors.ContainsKey(response))
                        AddEditor(response);

                    Editor editor = _editors[response];
                    CreateCachedEditor(response, response.GetType(), ref editor);

                    editor.OnInspectorGUI();

                    EditorGUI.EndDisabledGroup();

                    EditorGUILayout.Space();

                    EditorGUI.BeginDisabledGroup(!Application.isPlaying);
                    EditorGUILayout.BeginHorizontal();
                    {
                        if (GUILayout.Button("Play", EditorStyles.miniButtonMid))
                        {
                            PlayResponse(id);
                        }
                        if (GUILayout.Button("Stop", EditorStyles.miniButtonMid))
                        {
                            StopResponse(id);
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUI.EndDisabledGroup();

                    EditorGUILayout.Space();
                    EditorGUILayout.Space();
                }
            }

            // Draw add new item

            if (_riseResponses.arraySize > 0)
                RiseResponseStyling.DrawSplitter();

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            {
                // response list

                int newItem = EditorGUILayout.Popup(0, _typeDisplays) - 1;
                if (newItem >= 0)
                    AddResponse(_types[newItem]);

                // Paste response copy as new

                if (ResponseCopy.HasCopy())
                {
                    if (GUILayout.Button("Paste as new", EditorStyles.miniButton, GUILayout.Width(EditorStyles.miniButton.CalcSize(new GUIContent("Paste as new")).x)))
                        PasteAsNew();
                }
            }
            EditorGUILayout.EndHorizontal();

            // Reorder

            if (_draggedStartID >= 0 && _draggedEndID >= 0)
            {
                if (_draggedEndID != _draggedStartID)
                {
                    if (_draggedEndID > _draggedStartID)
                        _draggedEndID--;
                    _riseResponses.MoveArrayElement(_draggedStartID, _draggedEndID);
                    _draggedStartID = _draggedEndID;
                }
            }

            if (_draggedStartID >= 0 || _draggedEndID >= 0)
            {
                switch (e.type)
                {
                    case EventType.MouseUp:
                        _draggedStartID = -1;
                        _draggedEndID = -1;
                        e.Use();
                        break;
                    default:
                        break;
                }
            }

            // Clean up

            bool wasRemoved = false;
            for (int i = _riseResponses.arraySize - 1; i >= 0; i--)
            {
                if (_riseResponses.GetArrayElementAtIndex(i).objectReferenceValue == null)
                {
                    wasRemoved = true;
                    _riseResponses.DeleteArrayElementAtIndex(i);
                }
            }

            if (wasRemoved)
            {
                GameObject gameObject = (target as RiseResponses).gameObject;
                foreach (var c in gameObject.GetComponents<Component>())
                {
                    c.hideFlags = HideFlags.None;
                }
            }

            // Apply changes

            serializedObject.ApplyModifiedProperties();

            // Draw debug

            RiseResponseStyling.DrawSection("All Responses Debug");

            // Testing buttons

            EditorGUI.BeginDisabledGroup(!Application.isPlaying);
            EditorGUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Initialize", EditorStyles.miniButtonLeft))
                {
                    (target as RiseResponses).Initialization();
                }
                if (GUILayout.Button("Play", EditorStyles.miniButtonMid))
                {
                    (target as RiseResponses).PlayResponses();
                }
                if (GUILayout.Button("Stop", EditorStyles.miniButtonMid))
                {
                    (target as RiseResponses).StopResponses();
                }
                if (GUILayout.Button("Reset", EditorStyles.miniButtonMid))
                {
                    (target as RiseResponses).ResetResponses();
                }
                EditorGUI.BeginChangeCheck();
                {
                    _debugView = GUILayout.Toggle(_debugView, "Debug View", EditorStyles.miniButtonRight);

                    if (EditorGUI.EndChangeCheck())
                    {
                        foreach (var f in (target as RiseResponses).Responses)
                            f.hideFlags = _debugView ? HideFlags.HideInInspector : HideFlags.None;
                        UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
                    }
                }
            }
            EditorGUILayout.EndHorizontal();
            EditorGUI.EndDisabledGroup();

            // Debug draw

            

            if (_debugView)
            {
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.PropertyField(_riseResponses, true);
                EditorGUI.EndDisabledGroup();
            }
        }

        /// <summary>
        /// We need to repaint constantly if dragging a response around
        /// </summary>
        public override bool RequiresConstantRepaint()
        {
            return _draggedStartID >= 0;
        }

        /// <summary>
        /// Add a response to the list
        /// </summary>
        RiseResponse AddResponse(System.Type type)
        {
            GameObject gameObject = (target as RiseResponses).gameObject;

            RiseResponse newResponse = Undo.AddComponent(gameObject, type) as RiseResponse;
            newResponse.hideFlags = _debugView ? HideFlags.None : HideFlags.HideInInspector;
            newResponse.Label = ResponsePathAttributeAttribute.GetResponseDefaultName(type);
            //newFeedback.ResetFeedback();

            AddEditor(newResponse);

            _riseResponses.arraySize++;
            _riseResponses.GetArrayElementAtIndex(_riseResponses.arraySize - 1).objectReferenceValue = newResponse;

            return newResponse;
        }

        //
        // Editors management
        //

        /// <summary>
        /// Create the editor for a response
        /// </summary>
        void AddEditor(RiseResponse response)
        {
            if (response == null)
                return;

            if (!_editors.ContainsKey(response))
            {
                Editor editor = null;
                CreateCachedEditor(response, null, ref editor);

                _editors.Add(response, editor as Editor);
            }
        }

        /// <summary>
        /// Destroy the editor for a response
        /// </summary>
        void RemoveEditor(RiseResponse response)
        {
            if (response == null)
                return;

            if (_editors.ContainsKey(response))
            {
                DestroyImmediate(_editors[response]);
                _editors.Remove(response);
            }
        }

        //
        // response generic menus
        //

        /// <summary>
        /// Play the selected response
        /// </summary>
        void InitializeResponse(int id)
        {
            SerializedProperty property = _riseResponses.GetArrayElementAtIndex(id);
            RiseResponse response = property.objectReferenceValue as RiseResponse;
            response.Initialization(response.gameObject);
        }

        /// <summary>
        /// Play the selected response
        /// </summary>
        void PlayResponse(int id)
        {
            SerializedProperty property = _riseResponses.GetArrayElementAtIndex(id);
            RiseResponse response = property.objectReferenceValue as RiseResponse;
            response.Play(response.transform.position);
        }

        /// <summary>
        /// Play the selected response
        /// </summary>
        void StopResponse(int id)
        {
            SerializedProperty property = _riseResponses.GetArrayElementAtIndex(id);
            RiseResponse response = property.objectReferenceValue as RiseResponse;
            response.Stop(response.transform.position);
        }

        /// <summary>
        /// Resets the selected response
        /// </summary>
        /// <param name="id"></param>
        void ResetResponse(int id)
        {
            SerializedProperty property = _riseResponses.GetArrayElementAtIndex(id);
            RiseResponse response = property.objectReferenceValue as RiseResponse;
            response.ResetResponse();
        }
        
        /// <summary>
        /// Remove the selected feedback
        /// </summary>
        void RemoveResponse(int id)
        {
            SerializedProperty property = _riseResponses.GetArrayElementAtIndex(id);
            RiseResponse response = property.objectReferenceValue as RiseResponse;

            (target as RiseResponses).Responses.Remove(response);

            _editors.Remove(response);
            Undo.DestroyObjectImmediate(response);
        }

        /// <summary>
        /// Copy the selected response
        /// </summary>
        void CopyResponse(int id)
        {
            SerializedProperty property = _riseResponses.GetArrayElementAtIndex(id);
            RiseResponse response = property.objectReferenceValue as RiseResponse;

            ResponseCopy.Copy(new SerializedObject(response));
        }

        /// <summary>
        /// Paste the previously copied response values into the selected response
        /// </summary>
        void PasteResponse(int id)
        {
            SerializedProperty property = _riseResponses.GetArrayElementAtIndex(id);
            RiseResponse response = property.objectReferenceValue as RiseResponse;

            SerializedObject serialized = new SerializedObject(response);

            ResponseCopy.Paste(serialized);
            serialized.ApplyModifiedProperties();
        }

        /// <summary>
        /// Create a new response and apply the previoulsy copied response values
        /// </summary>
        void PasteAsNew()
        {
            RiseResponse newResponse = AddResponse(ResponseCopy.Type);

            SerializedObject serialized = new SerializedObject(newResponse);

            serialized.Update();
            ResponseCopy.Paste(serialized);
            serialized.ApplyModifiedProperties();
        }
    }
}