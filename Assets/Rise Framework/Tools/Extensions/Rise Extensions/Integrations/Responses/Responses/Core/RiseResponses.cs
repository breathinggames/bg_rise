﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BreathingGames.Response;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace BreathingGames.Response
{
    /// <summary>
    /// A collection of Responses, meant to be played altogether.
    /// This class provides a custom inspector to add and customize responses, and public methods to trigger them, stop them, etc.
    /// You can either use it on its own, or bind it from another class and trigger it from there.
    /// </summary>
    [ExecuteAlways]
    [DisallowMultipleComponent]
    public class RiseResponses : MonoBehaviour
    {
        /// a list of Responses to trigger
        public List<RiseResponse> Responses = new List<RiseResponse>();
        /// the possible initialization modes. If you use Script, you'll have to initialize manually by calling the Initialization method and passing it an owner
        /// Otherwise, you can have this component initialize itself at Awake or Start, and in this case the owner will be the responses itself
        public enum InitializationModes { Script, Awake, Start }
        /// the chosen initialization mode
        public InitializationModes InitializationMode = InitializationModes.Start;
        [HideInInspector]
        /// whether or not this Responses is in debug mode
        public bool DebugActive = false;
        /// whether or not this Responses is playing right now - meaning it hasn't been stopped yet.
        /// if you don't stop your Responses it'll remain true of course
        public bool IsPlaying { get; protected set; }

        /// <summary>
        /// On Awake we initialize our Responses if we're in auto mode
        /// </summary>
        protected virtual void Awake()
        {
            if ((InitializationMode == InitializationModes.Awake) && (Application.isPlaying))
            {
                Initialization(this.gameObject);
            }
        }

        /// <summary>
        /// On Start we initialize our Responses if we're in auto mode
        /// </summary>
        protected virtual void Start()
        {
            if ((InitializationMode == InitializationModes.Start) && (Application.isPlaying))
            {
                Initialization(this.gameObject);
            }
        }

        /// <summary>
        /// Initializes the Responses, setting this Responses as the owner
        /// </summary>
        public virtual void Initialization()
        {
            for (int i = 0; i < Responses.Count; i++)
            {

                Responses[i].Initialization(this.gameObject);
            }
        }

        /// <summary>
        /// A public method to initialize the Responses, specifying an owner that will be used as the reference for position and hierarchy by Responses
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="responsesOwner"></param>
        public virtual void Initialization(GameObject owner)
        {
            IsPlaying = false;
            for (int i = 0; i < Responses.Count; i++)
            {
                Responses[i].Initialization(owner);
            }
        }

        /// <summary>
        /// Plays all Responses using the Responses' position as reference, and no attenuation
        /// </summary>
        public virtual void PlayResponses()
        {
            IsPlaying = true;
            for (int i = 0; i < Responses.Count; i++)
            {
                Responses[i].Play(this.transform.position, 1.0f);
            }
        }

        /// <summary>
        /// Plays all Responses, specifying a position and attenuation. The position may be used by each Responses and taken into account to spark a particle or play a sound for example.
        /// The attenuation is a factor that can be used by each Responses to lower its intensity, usually you'll want to define that attenuation based on time or distance (using a lower 
        /// attenuation value for Responses happening further away from the Player).
        /// </summary>
        /// <param name="position"></param>
        /// <param name="responsesOwner"></param>
        /// <param name="attenuation"></param>
        public virtual void PlayResponses(Vector3 position, float attenuation = 1.0f)
        {
            IsPlaying = true;
            for (int i = 0; i < Responses.Count; i++)
            {
                Responses[i].Play(position, attenuation);
            }
        }

        /// <summary>
        /// Stops all Responses from playing. 
        /// </summary>
        public virtual void StopResponses()
        {
            for (int i = 0; i < Responses.Count; i++)
            {
                Responses[i].Stop(this.transform.position, 1.0f);
            }
            IsPlaying = false;
        }

        /// <summary>
        /// Stops all responses from playing, specifying a position and attenuation that can be used by the responses 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="attenuation"></param>
        public virtual void StopResponses(Vector3 position, float attenuation = 1.0f)
        {
            for (int i = 0; i < Responses.Count; i++)
            {
                Responses[i].Stop(position, attenuation);
            }
            IsPlaying = false;
        }

        /// <summary>
        /// Calls each Response's Reset method if they've defined one. An example of that can be resetting the initial color of a flickering renderer.
        /// </summary>
        public virtual void ResetResponses()
        {
            for (int i = 0; i < Responses.Count; i++)
            {
                Responses[i].ResetResponse();
            }
            IsPlaying = false;
        }

        /// <summary>
        /// On Destroy, removes all Responses from this Responses to avoid any leftovers
        /// </summary>
        protected virtual void OnDestroy()
        {
            IsPlaying = false;
            #if UNITY_EDITOR
            if (!Application.isPlaying)
            {            
                // we remove all binders
                foreach (RiseResponse response in Responses)
                {
                    EditorApplication.delayCall += () =>
                    {
                        DestroyImmediate(response);
                    };                    
                }
            }
            #endif
        }
            
    }

}
